//
//  Contact.swift
//  Yours
//
//  Created by Eduardo Arenas Albarracin on 26/05/16.
//  Copyright © 2016 Eduardo Arenas. All rights reserved.
//

import UIKit
import CoreData
import Contacts
import CGLAlphabetizer

/**
 Enum for Chat Entity member fields
 */
enum ContactAttributes : String {
    case
    id                  = "id",
    first_name          = "first_name",
    last_name           = "last_name",
    mobile_number       = "mobile_number",
    country_code        = "country_code",
    full_mobile_number  = "full_mobile_number",
    picture_url         = "picture_url",
    last_connected_at   = "last_connected_at",
    is_friend           = "is_friend"
    
    static let getAll = [
        id,
        first_name,
        last_name,
        mobile_number,
        country_code,
        full_mobile_number,
        picture_url,
        last_connected_at,
        is_friend
    ]
}

@objc(Contact)

/**
 The Core Data Model: Chat
 */
class Contact: NSManagedObject {
    private struct PropertyKey {
        static let kId: String = "id"
        static let kNameKey: String = "first_name"
        static let kMobileNumberKey: String = "mobile_number"
        static let kCountryCodeKey: String = "country_code"
        static let kFullMobileNumberKey: String = "full_mobile_number"
        static let kPictureURLKey: String = "picture_url"
        static let kLastConnectedAtKey: String = "last_connected_at"
    }
    
    @NSManaged var id: Int
    @NSManaged var first_name: String
    @NSManaged var last_name: String
    @NSManaged var mobile_number: String
    @NSManaged var country_code: String
    @NSManaged var full_mobile_number: String
    @NSManaged var picture_url: String
    @NSManaged var last_connected_at: NSDate
    @NSManaged var is_friend: Bool
}

extension Contact {
    func toJSON(withNativeValues: Bool = true) -> [String:AnyObject] {
        var dictionary: [String: AnyObject] = [String: AnyObject]()
        
        dictionary[PropertyKey.kId] = self.id
        dictionary[PropertyKey.kNameKey] = self.first_name
        dictionary[PropertyKey.kMobileNumberKey] = self.mobile_number
        dictionary[PropertyKey.kCountryCodeKey] = self.country_code
        dictionary[PropertyKey.kFullMobileNumberKey] = self.full_mobile_number
        dictionary[PropertyKey.kPictureURLKey] = self.picture_url
        dictionary[PropertyKey.kLastConnectedAtKey] = withNativeValues ? self.last_connected_at : DateFormatter.dateToString(self.last_connected_at)
        
        return dictionary;
    }
}

class ContactEntity: NSObject {
    private struct PropertyKey {
        static let kId: String = "id"
        static let kNameKey: String = "first_name"
        static let kMobileNumberKey: String = "mobile_number"
        static let kCountryCodeKey: String = "country_code"
        static let kFullMobileNumberKey: String = "full_mobile_number"
        static let kPictureURLKey: String = "picture_url"
        static let kLastConnectedAtKey: String = "last_connected_at"
    }
    
    var id: Int?
    var first_name: String?
    var lastName: String?
    var mobileNumber: String?
    var countryCode: String?
    var fullMobileNumber: String?
    var pictureURL: String?
    var lastConnectedAt: NSDate?
    var isFriend: Bool = false
    
    var identifier: String?
    var phoneNumbers: [[String: String]] = []
    
//    init(_id: Int, _first_name: String) {
//        id = _id
//        first_name = _first_name
//    }
    
    init(_identifier: String!, _firstName: String!, _lastName: String!) {
        identifier = _identifier
        first_name = _firstName
        lastName = _lastName
    }
    
    init(dictionary: [String: AnyObject]? = nil, isFriend: Bool? = false) {
        super.init()
        self.assignAttributes(dictionary, isFriend: isFriend!)
    }
    
    func assignAttributes(dictionary: [String: AnyObject]?, isFriend: Bool) {
        if (dictionary != nil) {
            if let _id = dictionary![PropertyKey.kId] as? Int {
                self.id = _id
            }
            if let _first_name = dictionary![PropertyKey.kNameKey] as? String {
                self.first_name = _first_name
            }
            if let _mobileNumber = dictionary![PropertyKey.kMobileNumberKey] as? String {
                self.mobileNumber = _mobileNumber
            }
            if let _countryCode = dictionary![PropertyKey.kCountryCodeKey] as? String {
                self.countryCode = _countryCode
            }
            if let _fullMobileNumber = dictionary![PropertyKey.kFullMobileNumberKey] as? String {
                self.fullMobileNumber = _fullMobileNumber
            }
            if let _pictureURL = dictionary![PropertyKey.kPictureURLKey] as? String {
                self.pictureURL = _pictureURL
            }
            if let _lastConnectedAt = dictionary![PropertyKey.kLastConnectedAtKey] as? String {
                self.lastConnectedAt = DateFormatter.stringToDate(_lastConnectedAt)
//                self.lastConnectedAt = NSDate(timeIntervalSince1970: _lastConnectedAt)
            }
            self.isFriend = isFriend
        }
    }
    
    // MARK: Helper methods
    
    func toJSON(withNativeValues: Bool = true) -> [String:AnyObject] {
        var dictionary: [String: AnyObject] = [String: AnyObject]()
        
        dictionary[PropertyKey.kId] = self.id
        dictionary[PropertyKey.kNameKey] = self.first_name
        dictionary[PropertyKey.kMobileNumberKey] = self.mobileNumber
        dictionary[PropertyKey.kCountryCodeKey] = self.countryCode
        dictionary[PropertyKey.kFullMobileNumberKey] = self.fullMobileNumber
        dictionary[PropertyKey.kPictureURLKey] = self.pictureURL
        dictionary[PropertyKey.kLastConnectedAtKey] = withNativeValues ? self.lastConnectedAt : DateFormatter.dateToString(self.lastConnectedAt!)
        
        return dictionary;
    }
    
    func fullName() -> String {
        var fullName = ""
        if first_name != nil {
            fullName += first_name!
        }
        if lastName != nil {
            fullName += " \(lastName!)"
        }
        return fullName
    }
    
    func lastConnectedDescription() -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "hh:mm (dd/MM)"
        if lastConnectedAt != nil {
            return "Last time at \(dateFormatter.stringFromDate(lastConnectedAt!))"
        } else {
            return "Online now"
        }
    }
    
    static func fetchContacts() -> [ContactEntity] {
        var contacts: [ContactEntity] = []
        if #available(iOS 9.0, *) {
            let contactStore = CNContactStore()
            do {
                let request: CNContactFetchRequest
                request = CNContactFetchRequest(keysToFetch: [CNContactPhoneNumbersKey, CNContactFamilyNameKey, CNContactGivenNameKey])
                request.sortOrder = CNContactSortOrder.GivenName
                try contactStore.enumerateContactsWithFetchRequest(request) { (contact, cursor) -> Void in
                    let _contact = ContactEntity(_identifier: contact.identifier, _firstName: contact.givenName, _lastName: contact.familyName)
                    contact.phoneNumbers.forEach({ (phoneNumber) in
                        _contact.phoneNumbers.append(["country_code": String(phoneNumber.valueForKey("value")!.valueForKey("countryCode")!), "mobile_number": String(phoneNumber.valueForKey("value")!.valueForKey("digits")!)])
                    })
                    contacts.append(_contact)
                }
            } catch {
                print("Handle the error please")
            }
        } else {
            // Fallback on earlier versions
        }
        return contacts
    }
    
    static func fetchDictionaryContacts() -> NSDictionary {
        return CGLAlphabetizer.alphabetizedDictionaryFromObjects(self.fetchContacts(), usingKeyPath: "lastName")
    }
    
    // MARK: - Core Data actions
    
    //    static func batchContactsFriends(json: [AnyObject]?) -> [Contact]{
    //        return batch(json, isFriend: true)
    //    }
    //
    //    static func batchContactsNoFriends(json: [AnyObject]?) -> [Contact]{
    //        return batch(json, isFriend: false)
    //    }
    //
    //    static func batch(json: [AnyObject]?, isFriend: Bool) -> [Contact]{
    //        var contacts: [Contact] = []
    //        for contactJSON in json! {
    //            contacts.append(ContactEntity(dictionary: contactJSON as? [String : AnyObject], isFriend: isFriend))
    //        }
    //        return contacts
    //    }
    
}
