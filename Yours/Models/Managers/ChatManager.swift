//
//  ChatManager.swift
//  Yours
//
//  Created by Eduardo Arenas Albarracin on 9/07/16.
//  Copyright © 2016 Eduardo Arenas. All rights reserved.
//

import UIKit
import CoreData

class ChatManager {
    
    private let persistenceManager: PersistenceManager!
    private var mainContextInstance:NSManagedObjectContext!
    
    private let idNamespace = ChatAttributes.id.rawValue
    private let roomNamespace = ChatAttributes.room.rawValue
    private let chatTypeNamespace = ChatAttributes.chat_type.rawValue
    private let nameNamespace = ChatAttributes.name.rawValue
    private let isAdminNamespace = ChatAttributes.is_admin.rawValue
    
    //Utilize Singleton pattern by instanciating ChatAPI only once.
    class var sharedInstance: ChatManager {
        struct Singleton {
            static let instance = ChatManager()
        }
        
        return Singleton.instance
    }
    
    init() {
        self.persistenceManager = PersistenceManager.sharedInstance
        self.mainContextInstance = persistenceManager.getMainContextInstance()
    }
    
    // MARK: Create
    
    /**
     Create a single Chat item, and persist it to Datastore via Worker(minion),
     that synchronizes with Main context.
     
     - Parameter ChatDetails: <Dictionary<String, AnyObject> A single Chat item to be persisted to the Datastore.
     - Returns: Void
     */
    func saveChat(chatDetails: Dictionary<String, AnyObject>) {
        
        //Minion Context worker with Private Concurrency type.
        let minionManagedObjectContextWorker:NSManagedObjectContext =
            NSManagedObjectContext.init(concurrencyType: NSManagedObjectContextConcurrencyType.PrivateQueueConcurrencyType)
        minionManagedObjectContextWorker.parentContext = self.mainContextInstance
        
        //Create new Object of Chat entity
        let chatItem = NSEntityDescription.insertNewObjectForEntityForName("Chat", inManagedObjectContext: minionManagedObjectContextWorker) as! Chat
        
        //Assign field values
        for (key, value) in chatDetails {
            for attribute in ChatAttributes.getAll {
                if (key == attribute.rawValue) {
                    chatItem.setValue(value, forKey: key)
                }
            }
        }
        let contactsList = chatDetails["users"] as! [Dictionary<String, AnyObject>]
//        let contactMutableSet: NSMutableSet = Chat.mutableSetValueForKey("contacts")
//        var _contacts: [Contact] = []
        for contactItem in contactsList {
            let contact = ContactManager.sharedInstance.saveContact(contactItem, context: minionManagedObjectContextWorker)
//            _contacts.append(contact)
//            contactMutableSet.addObject(contact)
            chatItem.addContact(contact)
        }
//        chatItem.contacts = NSSet(objects: _contacts)
//        chatItem.setValue(NSSet(objects: _contacts), forKey: "contacts")
//        chatItem.contacts = contactMutableSet
        
        //Save current work on Minion workers
        self.persistenceManager.saveWorkerContext(minionManagedObjectContextWorker)
        
        //Save and merge changes from Minion workers with Main context
        self.persistenceManager.mergeWithMainContext()
        
        //Post notification to update datasource of a given Viewcontroller/UITableView
        self.postUpdateNotification()
    }
    
    /**
     Create new Chats from a given list, and persist it to Datastore via Worker(minion),
     that synchronizes with Main context.
     
     - Parameter ChatsList: Array<AnyObject> Contains Chats to be persisted to the Datastore.
     - Returns: Void
     */
    func saveChatsList(chatsList:Array<AnyObject>){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), { () -> Void in
            
            //Minion Context worker with Private Concurrency type.
            let minionManagedObjectContextWorker:NSManagedObjectContext =
                NSManagedObjectContext.init(concurrencyType: NSManagedObjectContextConcurrencyType.PrivateQueueConcurrencyType)
            minionManagedObjectContextWorker.parentContext = self.mainContextInstance
            
            //Create ChatEntity, process member field values
            for index in 0..<chatsList.count {
                var chatItem:Dictionary<String, NSObject> = chatsList[index] as! Dictionary<String, NSObject>
                
                //Check that an Chat to be stored has a date, title and city.
                if chatItem[self.roomNamespace] != "" && chatItem[self.chatTypeNamespace] != "" {
                    
                    //Create new Object of Chat entity
                    let item = NSEntityDescription.insertNewObjectForEntityForName("Chat", inManagedObjectContext: minionManagedObjectContextWorker) as! Chat
                    
                    //Add member field values
                    item.setValue(chatItem[self.nameNamespace], forKey: self.nameNamespace)
                    item.setValue(chatItem[self.idNamespace], forKey: self.idNamespace)
                    item.setValue(chatItem[self.roomNamespace], forKey: self.roomNamespace)
                    item.setValue(chatItem[self.chatTypeNamespace], forKey: self.chatTypeNamespace)
                    item.setValue(chatItem[self.isAdminNamespace], forKey: self.isAdminNamespace)
                    
                    //Save current work on Minion workers
                    self.persistenceManager.saveWorkerContext(minionManagedObjectContextWorker)
                }
            }
            
            //Save and merge changes from Minion workers with Main context
            self.persistenceManager.mergeWithMainContext()
            
            //Post notification to update datasource of a given Viewcontroller/UITableView
            dispatch_async(dispatch_get_main_queue()) {
                self.postUpdateNotification()
            }
        })
    }
    
    // MARK: Read
    
    /**
     Retrieves all Chat items stored in the persistence layer, default (overridable)
     parameters:
     
     - Parameter sortedByDate: Bool flag to add sort rule: by Date
     - Parameter sortAscending: Bool flag to set rule on sorting: Ascending / Descending date.
     
     - Returns: Array<Chat> with found Chats in datastore
     */
    func getAllChats(sortedByDate:Bool = true, sortAscending:Bool = true) -> [Chat] {
        var fetchedResults:[Chat] = []
        
        // Create request on Chat entity
        let fetchRequest = NSFetchRequest(entityName: "Chat")
        
        //Create sort descriptor to sort retrieved Chats by Date, ascending
        if sortedByDate {
            let sortDescriptor = NSSortDescriptor(key: idNamespace, ascending: sortAscending)
            let sortDescriptors = [sortDescriptor]
            fetchRequest.sortDescriptors = sortDescriptors
        }
        
        //Execute Fetch request
        do {
            fetchedResults = try  self.mainContextInstance.executeFetchRequest(fetchRequest) as! [Chat]
        } catch let fetchError as NSError {
            print("retrieveById error: \(fetchError.localizedDescription)")
        }
        
        return fetchedResults
    }
    
    
    /**
     Retrieve an Chat, found by it's stored UUID.
     
     - Parameter ChatId: UUID of Chat item to retrieve
     - Returns: Array of Found Chat items, or empty Array
     */
    func getChatByRoom(room: String) -> Chat {
        var fetchedResults:Array<Chat> = Array<Chat>()
        
        // Create request on Chat entity
        let fetchRequest = NSFetchRequest(entityName: "Chat")
        
        //Add a predicate to filter by ChatId
        let findByRoomPredicate = NSPredicate(format: "\(roomNamespace) = %@", room)
        fetchRequest.predicate = findByRoomPredicate
        
        //Execute Fetch request
        do {
            fetchedResults = try self.mainContextInstance.executeFetchRequest(fetchRequest) as! [Chat]
        } catch let fetchError as NSError {
            print("retrieveById error: \(fetchError.localizedDescription)")
            fetchedResults = Array<Chat>()
        }
        
        return fetchedResults.first!
    }
    
    
    /**
     Retrieves all Chat items stored in the persistence layer
     and sort it by Date within a given range of (default) current date and
     (default)7 days from current date (is overridable, parameters are optional).
     
     - Parameter sortByDate: Bool default and overridable is set to True
     - Parameter sortAscending: Bool default and overridable is set to True
     - Parameter startDate: NSDate default and overridable is set to previous year
     - Parameter endDate: NSDate default and overridable is set to 1 week from current date
     - Returns: Array<Chat> with found Chats in datastore based on
     sort descriptor, in this case Date an dgiven date range.
     */
    
//    func getChatsInDateRange(sortByDate:Bool = true, sortAscending:Bool = true,
//                              startDate: NSDate = NSDate(timeInterval:-189216000, sinceDate:NSDate()),
//                              endDate: NSDate = NSCalendar.currentCalendar()
//        .dateByAddingUnit(
//            .Day,value: 7,
//            toDate: NSDate(),
//            options: NSCalendarOptions(rawValue: 0))!) -> Array<Chat> {
//        
//        // Create request on Chat entity
//        let fetchRequest = NSFetchRequest(entityName: EntityTypes.Chat.rawValue)
//        
//        //Create sort descriptor to sort retrieved Chats by Date, ascending
//        let sortDescriptor = NSSortDescriptor(key: ChatAttributes.date.rawValue,
//                                              ascending: sortAscending)
//        let sortDescriptors = [sortDescriptor]
//        fetchRequest.sortDescriptors = sortDescriptors
//        
//        //Create predicate to filter by start- / end date
//        let findByDateRangePredicate = NSPredicate(format: "(\(dateNamespace) >= %@) AND (\(dateNamespace) <= %@)", startDate, endDate)
//        fetchRequest.predicate = findByDateRangePredicate
//        
//        //Execute Fetch request
//        var fetchedResults = Array<Chat>()
//        do {
//            fetchedResults = try self.mainContextInstance.executeFetchRequest(fetchRequest) as! [Chat]
//        } catch let fetchError as NSError {
//            print("retrieveItemsSortedByDateInDateRange error: \(fetchError.localizedDescription)")
//        }
//        
//        return fetchedResults
//    }
    
    // MARK: Update
    
    /**
     Update all Chats (batch update) attendees list.
     
     Since privacy is always a concern to take into account,
     anonymise the attendees list for every Chat.
     
     - Returns: Void
     */
//    func anonimizeAttendeesList()  {
//        // Create a fetch request for the entity Chat
//        let fetchRequest = NSFetchRequest(entityName: "Chat")
//        
//        // Execute the fetch request
//        var fetchedResults = Array<Chat>()
//        do {
//            fetchedResults = try self.mainContextInstance.executeFetchRequest(fetchRequest) as! [Chat]
//            
//            for Chat in fetchedResults {
//                //get count of current attendees list
//                let currCount = (Chat as Chat).attendees.count
//                
//                //Create an anonymised list of attendees
//                //with count of current attendees list
//                let anonymisedList = [String](count: currCount, repeatedValue: "Anonymous")
//                
//                //Update current attendees list with anonymised list, shallow copy.
//                (Chat as Chat).attendees = anonymisedList
//            }
//        } catch let updateError as NSError {
//            print("updateAllChatAttendees error: \(updateError.localizedDescription)")
//        }
//    }
    
    /**
     Update Chat item for specific keys.
     
     - Parameter ChatItemToUpdate: Chat the passed Chat to update it's member fields
     - Parameter newChatItemDetails: Dictionary<String,AnyObject> the details to be updated
     - Returns: Void
     */
    func updateChat(ChatItemToUpdate: Chat, newChatItemDetails: Dictionary<String, AnyObject>){
        
        let minionManagedObjectContextWorker:NSManagedObjectContext =
            NSManagedObjectContext.init(concurrencyType: NSManagedObjectContextConcurrencyType.PrivateQueueConcurrencyType)
        minionManagedObjectContextWorker.parentContext = self.mainContextInstance
        
        //Assign field values
        for (key, value) in newChatItemDetails {
            for attribute in ChatAttributes.getAll {
                if (key == attribute.rawValue) {
                    ChatItemToUpdate.setValue(value, forKey: key)
                }
            }
        }
        
        //Persist new Chat to datastore (via Managed Object Context Layer).
        self.persistenceManager.saveWorkerContext(minionManagedObjectContextWorker)
        self.persistenceManager.mergeWithMainContext()
        
        self.postUpdateNotification()
    }
    
    // MARK: Delete
    
    /**
     Delete all Chat items from persistence layer.
     
     - Returns: Void
     */
    func deleteAllChats() {
        let retrievedItems = getAllChats()
        
        //Delete all Chat items from persistance layer
        for item in retrievedItems {
            self.mainContextInstance.deleteObject(item)
        }
        self.persistenceManager.mergeWithMainContext()
        
        self.postUpdateNotification()
    }
    
    /**
     Delete a single Chat item from persistence layer.
     
     - Parameter ChatItem: Chat to be deleted
     - Returns: Void
     */
    func deleteChat(chatItem: Chat) {
        //Delete Chat item from persistance layer
        self.mainContextInstance.deleteObject(chatItem)
        self.postUpdateNotification()
    }
    
    /**
     Post update notification to let the registered listeners refresh it's datasource.
     
     - Returns: Void
     */
    private func postUpdateNotification(){
        NSNotificationCenter.defaultCenter().postNotificationName("updateChatTableData", object: nil)
    }

}

