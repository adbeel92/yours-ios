//
//  ContactManager.swift
//  Yours
//
//  Created by Eduardo Arenas Albarracin on 10/07/16.
//  Copyright © 2016 Eduardo Arenas. All rights reserved.
//

import UIKit
import CoreData

class ContactManager {
    
    private let persistenceManager: PersistenceManager!
    private var mainContextInstance:NSManagedObjectContext!
    
    private let idNamespace = ContactAttributes.id.rawValue
    private let firstNameNamespace = ContactAttributes.first_name.rawValue
    private let lastNameNamespace = ContactAttributes.last_name.rawValue
    private let mobileNumberNamespace = ContactAttributes.mobile_number.rawValue
    private let countryCodeNamespace = ContactAttributes.country_code.rawValue
    private let fullMobileNumberNamespace = ContactAttributes.full_mobile_number.rawValue
    private let pictureUrlNamespace = ContactAttributes.picture_url.rawValue
    private let lastConnectedAtNamespace = ContactAttributes.last_connected_at.rawValue
    private let isFriendNamespace = ContactAttributes.is_friend.rawValue

    //Utilize Singleton pattern by instanciating ContactAPI only once.
    class var sharedInstance: ContactManager {
        struct Singleton {
            static let instance = ContactManager()
        }
        
        return Singleton.instance
    }
    
    init() {
        self.persistenceManager = PersistenceManager.sharedInstance
        self.mainContextInstance = persistenceManager.getMainContextInstance()
    }
    
    // MARK: Create
    
    /**
     Create a single Contact item, and persist it to Datastore via Worker(minion),
     that synchronizes with Main context.
     
     - Parameter ContactDetails: <Dictionary<String, AnyObject> A single Contact item to be persisted to the Datastore.
     - Returns: Void
     */
    func saveContact(contactDetails: Dictionary<String, AnyObject>, context: NSManagedObjectContext?=nil) -> Contact {
        
        
        //Minion Context worker with Private Concurrency type.
        let minionManagedObjectContextWorker:NSManagedObjectContext
        if (context != nil) {
            minionManagedObjectContextWorker = context!
        } else {
            minionManagedObjectContextWorker = NSManagedObjectContext.init(concurrencyType: NSManagedObjectContextConcurrencyType.PrivateQueueConcurrencyType)
            minionManagedObjectContextWorker.parentContext = self.mainContextInstance
        }
        
        //Create new Object of Contact entity
        let contactItem = NSEntityDescription.insertNewObjectForEntityForName("Contact", inManagedObjectContext: minionManagedObjectContextWorker) as! Contact
        
        //Assign field values
        for (key, value) in contactDetails {
            for attribute in ContactAttributes.getAll {
                if (key == attribute.rawValue) {
                    contactItem.setValue(value, forKey: key)
                }
            }
        }
        
//        //Save current work on Minion workers
//        self.persistenceManager.saveWorkerContext(minionManagedObjectContextWorker)
//        
//        //Save and merge changes from Minion workers with Main context
//        self.persistenceManager.mergeWithMainContext()
//        
//        //Post notification to update datasource of a given Viewcontroller/UITableView
//        self.postUpdateNotification()
        
        return contactItem
    }
    
    /**
     Create new Contacts from a given list, and persist it to Datastore via Worker(minion),
     that synchronizes with Main context.
     
     - Parameter ContactsList: Array<AnyObject> Contains Contacts to be persisted to the Datastore.
     - Returns: Void
     */
    func saveContactsList(contactsList:Array<AnyObject>){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), { () -> Void in
            
            //Minion Context worker with Private Concurrency type.
            let minionManagedObjectContextWorker:NSManagedObjectContext =
                NSManagedObjectContext.init(concurrencyType: NSManagedObjectContextConcurrencyType.PrivateQueueConcurrencyType)
            minionManagedObjectContextWorker.parentContext = self.mainContextInstance
            
            //Create ContactEntity, process member field values
            for index in 0..<contactsList.count {
                var contactItem:Dictionary<String, NSObject> = contactsList[index] as! Dictionary<String, NSObject>
                
                //Check that an Contact to be stored has a date, title and city.
                if contactItem[self.mobileNumberNamespace] != "" {
                    
                    //Create new Object of Contact entity
                    let item = NSEntityDescription.insertNewObjectForEntityForName("Contact", inManagedObjectContext: minionManagedObjectContextWorker) as! Contact
                    
                    //Add member field values
                    item.setValue(contactItem[self.idNamespace], forKey: self.idNamespace)
                    item.setValue(contactItem[self.firstNameNamespace], forKey: self.firstNameNamespace)
                    item.setValue(contactItem[self.lastNameNamespace], forKey: self.lastNameNamespace)
                    item.setValue(contactItem[self.mobileNumberNamespace], forKey: self.mobileNumberNamespace)
                    item.setValue(contactItem[self.countryCodeNamespace], forKey: self.countryCodeNamespace)
                    item.setValue(contactItem[self.fullMobileNumberNamespace], forKey: self.fullMobileNumberNamespace)
                    item.setValue(contactItem[self.pictureUrlNamespace], forKey: self.pictureUrlNamespace)
                    item.setValue(contactItem[self.lastConnectedAtNamespace], forKey: self.lastConnectedAtNamespace)
                    item.setValue(contactItem[self.isFriendNamespace], forKey: self.isFriendNamespace)
                    
                    //Save current work on Minion workers
                    self.persistenceManager.saveWorkerContext(minionManagedObjectContextWorker)
                }
            }
            
            //Save and merge changes from Minion workers with Main context
            self.persistenceManager.mergeWithMainContext()
            
            //Post notification to update datasource of a given Viewcontroller/UITableView
            dispatch_async(dispatch_get_main_queue()) {
                self.postUpdateNotification()
            }
        })
    }
    
    // MARK: Read
    
    /**
     Retrieves all Contact items stored in the persistence layer, default (overridable)
     parameters:
     
     - Parameter sortedByDate: Bool flag to add sort rule: by Date
     - Parameter sortAscending: Bool flag to set rule on sorting: Ascending / Descending date.
     
     - Returns: Array<Contact> with found Contacts in datastore
     */
    func getAllContacts(sortedByDate:Bool = true, sortAscending:Bool = true) -> [Contact] {
        var fetchedResults:[Contact] = []
        
        // Create request on Contact entity
        let fetchRequest = NSFetchRequest(entityName: "Contact")
        
        //Create sort descriptor to sort retrieved Contacts by Date, ascending
        if sortedByDate {
            let sortDescriptor = NSSortDescriptor(key: idNamespace, ascending: sortAscending)
            let sortDescriptors = [sortDescriptor]
            fetchRequest.sortDescriptors = sortDescriptors
        }
        
        //Execute Fetch request
        do {
            fetchedResults = try  self.mainContextInstance.executeFetchRequest(fetchRequest) as! [Contact]
        } catch let fetchError as NSError {
            print("retrieveById error: \(fetchError.localizedDescription)")
        }
        
        return fetchedResults
    }
    
    
    /**
     Retrieve an Contact, found by it's stored UUID.
     
     - Parameter ContactId: UUID of Contact item to retrieve
     - Returns: Array of Found Contact items, or empty Array
     */
//    func getContactById(room: String) -> Contact {
//        var fetchedResults:Array<Contact> = Array<Contact>()
//        
//        // Create request on Contact entity
//        let fetchRequest = NSFetchRequest(entityName: "Contact")
//        
//        //Add a predicate to filter by ContactId
//        let findByRoomPredicate = NSPredicate(format: "\(roomNamespace) = %@", room)
//        fetchRequest.predicate = findByRoomPredicate
//        
//        //Execute Fetch request
//        do {
//            fetchedResults = try self.mainContextInstance.executeFetchRequest(fetchRequest) as! [Contact]
//        } catch let fetchError as NSError {
//            print("retrieveById error: \(fetchError.localizedDescription)")
//            fetchedResults = Array<Contact>()
//        }
//        
//        return fetchedResults.first!
//    }
    
    
    /**
     Retrieves all Contact items stored in the persistence layer
     and sort it by Date within a given range of (default) current date and
     (default)7 days from current date (is overridable, parameters are optional).
     
     - Parameter sortByDate: Bool default and overridable is set to True
     - Parameter sortAscending: Bool default and overridable is set to True
     - Parameter startDate: NSDate default and overridable is set to previous year
     - Parameter endDate: NSDate default and overridable is set to 1 week from current date
     - Returns: Array<Contact> with found Contacts in datastore based on
     sort descriptor, in this case Date an dgiven date range.
     */
    
    //    func getContactsInDateRange(sortByDate:Bool = true, sortAscending:Bool = true,
    //                              startDate: NSDate = NSDate(timeInterval:-189216000, sinceDate:NSDate()),
    //                              endDate: NSDate = NSCalendar.currentCalendar()
    //        .dateByAddingUnit(
    //            .Day,value: 7,
    //            toDate: NSDate(),
    //            options: NSCalendarOptions(rawValue: 0))!) -> Array<Contact> {
    //
    //        // Create request on Contact entity
    //        let fetchRequest = NSFetchRequest(entityName: EntityTypes.Contact.rawValue)
    //
    //        //Create sort descriptor to sort retrieved Contacts by Date, ascending
    //        let sortDescriptor = NSSortDescriptor(key: ContactAttributes.date.rawValue,
    //                                              ascending: sortAscending)
    //        let sortDescriptors = [sortDescriptor]
    //        fetchRequest.sortDescriptors = sortDescriptors
    //
    //        //Create predicate to filter by start- / end date
    //        let findByDateRangePredicate = NSPredicate(format: "(\(dateNamespace) >= %@) AND (\(dateNamespace) <= %@)", startDate, endDate)
    //        fetchRequest.predicate = findByDateRangePredicate
    //
    //        //Execute Fetch request
    //        var fetchedResults = Array<Contact>()
    //        do {
    //            fetchedResults = try self.mainContextInstance.executeFetchRequest(fetchRequest) as! [Contact]
    //        } catch let fetchError as NSError {
    //            print("retrieveItemsSortedByDateInDateRange error: \(fetchError.localizedDescription)")
    //        }
    //
    //        return fetchedResults
    //    }
    
    // MARK: Update
    
    /**
     Update all Contacts (batch update) attendees list.
     
     Since privacy is always a concern to take into account,
     anonymise the attendees list for every Contact.
     
     - Returns: Void
     */
    //    func anonimizeAttendeesList()  {
    //        // Create a fetch request for the entity Contact
    //        let fetchRequest = NSFetchRequest(entityName: "Contact")
    //
    //        // Execute the fetch request
    //        var fetchedResults = Array<Contact>()
    //        do {
    //            fetchedResults = try self.mainContextInstance.executeFetchRequest(fetchRequest) as! [Contact]
    //
    //            for Contact in fetchedResults {
    //                //get count of current attendees list
    //                let currCount = (Contact as Contact).attendees.count
    //
    //                //Create an anonymised list of attendees
    //                //with count of current attendees list
    //                let anonymisedList = [String](count: currCount, repeatedValue: "Anonymous")
    //
    //                //Update current attendees list with anonymised list, shallow copy.
    //                (Contact as Contact).attendees = anonymisedList
    //            }
    //        } catch let updateError as NSError {
    //            print("updateAllContactAttendees error: \(updateError.localizedDescription)")
    //        }
    //    }
    
    /**
     Update Contact item for specific keys.
     
     - Parameter ContactItemToUpdate: Contact the passed Contact to update it's member fields
     - Parameter newContactItemDetails: Dictionary<String,AnyObject> the details to be updated
     - Returns: Void
     */
    func updateContact(contactItemToUpdate: Contact, newContactItemDetails: Dictionary<String, AnyObject>){
        
        let minionManagedObjectContextWorker:NSManagedObjectContext =
            NSManagedObjectContext.init(concurrencyType: NSManagedObjectContextConcurrencyType.PrivateQueueConcurrencyType)
        minionManagedObjectContextWorker.parentContext = self.mainContextInstance
        
        //Assign field values
        for (key, value) in newContactItemDetails {
            for attribute in ContactAttributes.getAll {
                if (key == attribute.rawValue) {
                    contactItemToUpdate.setValue(value, forKey: key)
                }
            }
        }
        
        //Persist new Contact to datastore (via Managed Object Context Layer).
        self.persistenceManager.saveWorkerContext(minionManagedObjectContextWorker)
        self.persistenceManager.mergeWithMainContext()
        
        self.postUpdateNotification()
    }
    
    // MARK: Delete
    
    /**
     Delete all Contact items from persistence layer.
     
     - Returns: Void
     */
    func deleteAllContacts() {
        let retrievedItems = getAllContacts()
        
        //Delete all Contact items from persistance layer
        for item in retrievedItems {
            self.mainContextInstance.deleteObject(item)
        }
        self.persistenceManager.mergeWithMainContext()
        
        self.postUpdateNotification()
    }
    
    /**
     Delete a single Contact item from persistence layer.
     
     - Parameter ContactItem: Contact to be deleted
     - Returns: Void
     */
    func deleteContact(ContactItem: Contact) {
        //Delete Contact item from persistance layer
        self.mainContextInstance.deleteObject(ContactItem)
        self.postUpdateNotification()
    }
    
    /**
     Post update notification to let the registered listeners refresh it's datasource.
     
     - Returns: Void
     */
    private func postUpdateNotification(){
        NSNotificationCenter.defaultCenter().postNotificationName("updateContactTableData", object: nil)
    }
    
}
