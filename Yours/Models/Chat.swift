//
//  Chat.swift
//  Yours
//
//  Created by Eduardo Arenas Albarracin on 15/05/16.
//  Copyright © 2016 Eduardo Arenas. All rights reserved.
//

import UIKit
import CoreData

/**
 Enum for Chat Entity member fields
 */
enum ChatAttributes : String {
    case
    id          = "id",
    room        = "room",
    chat_type   = "chat_type",
    name        = "name",
    is_admin    = "is_admin"
    
    static let getAll = [
        id,
        room,
        chat_type,
        name,
        is_admin
    ]
}

@objc(Chat)

/**
 The Core Data Model: Chat
 */
class Chat: NSManagedObject {
    @NSManaged var id: Int64
    @NSManaged var room: String
    @NSManaged var chat_type: String
    @NSManaged var name: String
    @NSManaged var is_admin: Bool
    @NSManaged var contacts: NSSet // It does not include current user
}

extension Chat {
    func addContact(contact: Contact) {
        let _contacts = self.mutableSetValueForKey("contacts")
        _contacts.addObject(contact)
    }
}

class ChatEntity: NSObject {
    private struct PropertyKey {
        static let kIdKey: String = "id"
        static let kRoomKey: String = "room"
        static let kChatTypeKey: String = "chat_type"
        static let kNameKey: String = "name"
        static let kIsAdminKey: String = "is_admin"
        static let kUsersKey: String = "users"
    }
    
    var id: Int?
    var room: String?
    var chatType: String?
    var name: String?
    var isAdmin: Bool = false
    var contacts: [ContactEntity] = [] // It does not include current user

    init(dictionary: [String: AnyObject]? = nil) {
        super.init()
        self.assignAttributes(dictionary)
    }
    
    func assignAttributes(dictionary: [String: AnyObject]?) {
        if (dictionary != nil) {
            if let _id = dictionary![PropertyKey.kIdKey] as? Int {
                self.id = _id
            }
            if let _room = dictionary![PropertyKey.kRoomKey] as? String {
                self.room = _room
            }
            if let _chatType = dictionary![PropertyKey.kChatTypeKey] as? String {
                self.chatType = _chatType
            }
            if let _name = dictionary![PropertyKey.kNameKey] as? String {
                self.name = _name
            }
            if let _isAdmin = dictionary![PropertyKey.kIsAdminKey] as? Bool {
                self.isAdmin = _isAdmin
            }
            if let _users: [[String : AnyObject]] = dictionary![PropertyKey.kUsersKey] as? [[String : AnyObject]] {
                self.contacts = []
                for _user in _users {
                    if ((_user["id"] as? Int) != User.currentUser()!.id) {
                        self.contacts.append(ContactEntity(dictionary: _user, isFriend: false))
                    }
                }
            }
            if let _users: [Contact] = dictionary![PropertyKey.kUsersKey] as? [Contact] {
                self.contacts = []
                for _user in _users {
                    if (_user.id != User.currentUser()!.id) {
                        let contact = ContactEntity(dictionary: _user.toJSON(false), isFriend: true)
                        self.contacts.append(contact)
                    }
                }
            }
        }
    }
    
    // MARK: Helper methods
    
    func toJSON() -> [String:AnyObject] {
        var dictionary: [String: AnyObject] = [String: AnyObject]()
        
        dictionary[PropertyKey.kIdKey] = self.id
        dictionary[PropertyKey.kRoomKey] = self.room
        dictionary[PropertyKey.kChatTypeKey] = self.chatType
        dictionary[PropertyKey.kNameKey] = self.name
        dictionary[PropertyKey.kIsAdminKey] = self.isAdmin
        dictionary[PropertyKey.kUsersKey] = self.contacts.map({ $0.toJSON() })
        
        return dictionary;
    }
    
    func title() -> String {
        if isChatGroup() {
            return self.name!
        } else {
            let contact: ContactEntity = self.contacts.first!
            if (contact.isFriend) {
                return contact.first_name!
            } else {
                return contact.fullMobileNumber!
            }
        }
    }
    
    func isChatGroup() -> Bool {
        return self.chatType == "grouping"
    }
    
    // MARK: - Core Data actions
    
    func saveIfNotExists() {
        let chat = findByRoom(self.room!)
        if (chat == nil) {
            self.save()
        }
    }
    
    func findByRoom(room: String) -> Chat? {
        return ChatManager.sharedInstance.getChatByRoom(room)
    }
    
    func save() {
        ChatManager.sharedInstance.saveChat(self.toJSON())
    }
    
    static func getAll() -> [ChatEntity] {
        let chats: [Chat] = ChatManager.sharedInstance.getAllChats()
        var _chats: [ChatEntity] = []
        for chat in chats {
            let dictionary: [String : AnyObject] = [PropertyKey.kIdKey: Int(chat.id), PropertyKey.kRoomKey: chat.room, PropertyKey.kChatTypeKey: chat.chat_type, PropertyKey.kNameKey: chat.name, PropertyKey.kIsAdminKey: chat.is_admin, PropertyKey.kUsersKey: (chat.contacts.allObjects as! [Contact])]
            let chatEntity = ChatEntity(dictionary: dictionary)
            _chats.append(chatEntity)
        }
        return _chats
    }

}