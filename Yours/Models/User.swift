//
//  User.swift
//  Yours
//
//  Created by Eduardo Arenas Albarracin on 17/04/16.
//  Copyright © 2016 Eduardo Arenas. All rights reserved.
//

import Foundation

let UserCurrentUserKey = "CurrentUser"

class User: NSObject {
    
    static let UserDidSignInNotification = "UserDidSignIn"
    static let UserDidSignOutNotification = "UserDidSignOut"
    
    private struct PropertyKey {
        static let kId: String = "id"
        static let kNameKey: String = "first_name"
        static let kAccessToken: String = "access_token"
        static let kMobileNumberKey: String = "mobile_number"
        static let kCountryCodeKey: String = "country_code"
        static let kFullMobileNumberKey: String = "full_mobile_number"
        static let kPictureURLKey: String = "picture_url"
    }
    
    var id: Int?
    var name: String?
    var accessToken: String = ""
    var mobileNumber: String?
    var countryCode: String?
    var fullMobileNumber: String?
    var pictureURL: String?
    
    init(_id: Int, _name: String) {
        id = _id
        name = _name
    }
    
    init(dictionary: [String: AnyObject]? = nil) {
        super.init()
        self.assignAttributes(dictionary)
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.id = aDecoder.decodeObjectForKey(PropertyKey.kId) as? Int
        self.name = aDecoder.decodeObjectForKey(PropertyKey.kNameKey) as? String
        self.accessToken = aDecoder.decodeObjectForKey(PropertyKey.kAccessToken) as! String
        self.mobileNumber = aDecoder.decodeObjectForKey(PropertyKey.kMobileNumberKey) as? String
        self.countryCode = aDecoder.decodeObjectForKey(PropertyKey.kCountryCodeKey) as? String
        self.fullMobileNumber = aDecoder.decodeObjectForKey(PropertyKey.kFullMobileNumberKey) as? String
        self.pictureURL = aDecoder.decodeObjectForKey(PropertyKey.kPictureURLKey) as? String
    }
    
    func encodeWithCoder(aCode:NSCoder) {
        if let id: Int = self.id {
            aCode.encodeObject(id, forKey: PropertyKey.kId)
        }
        if let name: String = self.name {
            aCode.encodeObject(name, forKey: PropertyKey.kNameKey)
        }
        if let accessToken: String = self.accessToken {
            aCode.encodeObject(accessToken, forKey: PropertyKey.kAccessToken)
        }
        if let mobileNumber: String = self.mobileNumber {
            aCode.encodeObject(mobileNumber, forKey: PropertyKey.kMobileNumberKey)
        }
        if let countryCode: String = self.countryCode {
            aCode.encodeObject(countryCode, forKey: PropertyKey.kCountryCodeKey)
        }
        if let fullMobileNumber: String = self.fullMobileNumber {
            aCode.encodeObject(fullMobileNumber, forKey: PropertyKey.kFullMobileNumberKey)
        }
        if let pictureURL: String = self.pictureURL {
            aCode.encodeObject(pictureURL, forKey: PropertyKey.kPictureURLKey)
        }
    }
    
    func assignAttributes(dictionary: [String: AnyObject]?) {
        if (dictionary != nil) {
            if let _id = dictionary![PropertyKey.kId] as? Int {
                self.id = _id
            }
            if let _name = dictionary![PropertyKey.kNameKey] as? String {
                self.name = _name
            }
            if let _accessToken = dictionary![PropertyKey.kAccessToken] as? String {
                self.accessToken = _accessToken
            }
            if let _mobileNumber = dictionary![PropertyKey.kMobileNumberKey] as? String {
                self.mobileNumber = _mobileNumber
            }
            if let _countryCode = dictionary![PropertyKey.kCountryCodeKey] as? String {
                self.countryCode = _countryCode
            }
            if let _fullMobileNumber = dictionary![PropertyKey.kFullMobileNumberKey] as? String {
                self.fullMobileNumber = _fullMobileNumber
            }
            if let _pictureURL = dictionary![PropertyKey.kPictureURLKey] as? String {
                self.pictureURL = _pictureURL
            }
        }
    }
    
    func toJSON() -> [String:AnyObject] {
        var dictionary: [String: AnyObject] = [String: AnyObject]()
        
        dictionary[PropertyKey.kId] = self.id
        dictionary[PropertyKey.kNameKey] = self.name
        dictionary[PropertyKey.kAccessToken] = self.accessToken
        dictionary[PropertyKey.kMobileNumberKey] = self.mobileNumber
        dictionary[PropertyKey.kCountryCodeKey] = self.countryCode
        dictionary[PropertyKey.kFullMobileNumberKey] = self.fullMobileNumber
        dictionary[PropertyKey.kPictureURLKey] = self.pictureURL
        
        return dictionary;
    }
    
    func save(){
        let encodedObject = NSKeyedArchiver.archivedDataWithRootObject(self)
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(encodedObject, forKey:UserCurrentUserKey)
        defaults.setBool(true, forKey:"signedIn?")
        defaults.synchronize()
    }
    
    func save(attributes: [String: AnyObject]) {
        if(attributes.count > 0){
            assignAttributes(attributes)
        }
        save()
    }
    
    func delete() {
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.removeObjectForKey(UserCurrentUserKey)
        defaults.setBool(false, forKey:"signedIn?")
        defaults.synchronize()
    }
    
    static func currentUser() -> User? {
        let defaults = NSUserDefaults.standardUserDefaults()
        let encodedObject = defaults.objectForKey(UserCurrentUserKey) as? NSData
        if encodedObject != nil {
            return (NSKeyedUnarchiver.unarchiveObjectWithData(encodedObject!) as! User)
        }
        return nil
    }
    
    static func isUserSignedIn() -> Bool {
        return NSUserDefaults.standardUserDefaults().boolForKey("signedIn?")
    }
    
}