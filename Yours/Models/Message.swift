//
//  Message.swift
//  Yours
//
//  Created by Eduardo Arenas Albarracin on 28/05/16.
//  Copyright © 2016 Eduardo Arenas. All rights reserved.
//

import Foundation

class Message: NSObject {
    private struct PropertyKey {
        static let kBodyKey: String = "message"
        static let kSenderKey: String = "sender"
        static let kSocketRoomKey: String = "toRoom"
    }
    
    var body: String!
    var sender: ContactEntity!
    var socketRoom: String!
    
    init(_body: String, _sender: ContactEntity, _socketRoom: String) {
        body = _body
        sender = _sender
        socketRoom = _socketRoom
    }
    
    init(dictionary: [String: AnyObject]? = nil) {
        super.init()
        self.assignAttributes(dictionary)
    }
    
    func assignAttributes(dictionary: [String: AnyObject]?) {
        if (dictionary != nil) {
            if let _body = dictionary![PropertyKey.kBodyKey] as? String {
                self.body = _body
            }
            if let _senderJSON = dictionary![PropertyKey.kSenderKey] as? [String : AnyObject] {
                self.sender = ContactEntity(dictionary: _senderJSON, isFriend: true)
            }
            if let _socketRoom = dictionary![PropertyKey.kSocketRoomKey] as? String {
                self.socketRoom = _socketRoom
            }
        }
    }
    
    func toJSON() -> [String:AnyObject] {
        var dictionary: [String: AnyObject] = [String: AnyObject]()
        
        dictionary[PropertyKey.kBodyKey] = self.body
        dictionary[PropertyKey.kSenderKey] = self.sender.toJSON()
        dictionary[PropertyKey.kSocketRoomKey] = self.socketRoom
        
        return dictionary;
    }
}