//
//  AppDelegate.swift
//  Yours
//
//  Created by Eduardo Arenas Albarracin on 11/04/16.
//  Copyright © 2016 Eduardo Arenas. All rights reserved.
//

import UIKit
import SocketIOClientSwift

let UINavigationBarAppeareanceActive = "UINavigationBarAppeareanceActive"
let UINavigationBarAppeareanceInative = "UINavigationBarAppeareanceInative"
let UINavigationBarAppeareanceNone = "UINavigationBarAppeareanceNone"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    lazy var datastoreCoordinator: DatastoreCoordinator = {
        return DatastoreCoordinator()
    }()
    
    lazy var contextManager: ContextManager = {
        return ContextManager()
    }()
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        setupInitialViewController()
        registerForUserNotifications()
        print("CURRENT USER => \(User.currentUser()?.toJSON())")
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        if User.isUserSignedIn() {
            SocketIOManager.sharedInstance.closeLogicalConnection()
        }
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        if User.isUserSignedIn() {
            SocketIOManager.sharedInstance.establishLogicalConnection()
        }
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
    }
    
    func setupInitialViewController() {
        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        let storyboard: UIStoryboard
        
        if User.isUserSignedIn() {
            storyboard = UIStoryboard(name: "Main", bundle: nil)
            let tabBarController = storyboard.instantiateInitialViewController() as! UITabBarController
            tabBarController.tabBar.tintColor = UIColor.globalTintColor()
            self.window?.rootViewController = tabBarController
            self.updateNavigationBarAppeareance(UINavigationBarAppeareanceActive)
        }
        else {
            storyboard = UIStoryboard(name: "Auth", bundle: nil)
            
            let initialViewController = storyboard.instantiateInitialViewController()
            self.window?.rootViewController = initialViewController
            self.updateNavigationBarAppeareance(UINavigationBarAppeareanceActive)
        }
        window?.makeKeyAndVisible()
        
    }
    
    func updateNavigationBarAppeareance(navigationBarAppeareance: String) {
        switch navigationBarAppeareance {
        case UINavigationBarAppeareanceActive:
            UINavigationBar.appearance().barTintColor = UIColor.globalTintColor()
            UINavigationBar.appearance().tintColor = UIColor.whiteColor()
            UINavigationBar.appearance().titleTextAttributes = [
                NSForegroundColorAttributeName: UIColor.whiteColor(),
                NSFontAttributeName: FontHelper.SFUITextMediumFontWithSize(18)
            ]
        case UINavigationBarAppeareanceInative:
            UINavigationBar.appearance().barTintColor = UIColor.initWithRGB(155, green: 155, blue: 155)
            UINavigationBar.appearance().tintColor = UIColor.whiteColor()
            UINavigationBar.appearance().titleTextAttributes = [
                NSForegroundColorAttributeName: UIColor.whiteColor(),
                NSFontAttributeName: FontHelper.SFUITextMediumFontWithSize(18)
            ]
        default:
            break;
        }
    }
    
    func registerForUserNotifications() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AppDelegate.userDidSignInNotification(_:)), name: User.UserDidSignInNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AppDelegate.userDidSignOutNotification(_:)), name: User.UserDidSignOutNotification, object: nil)
    }
    
    func userDidSignInNotification(notification: NSNotification) {
        
        NSOperationQueue.mainQueue().addOperationWithBlock { [unowned self] in
            let screenshotView = self.window!.snapshotViewAfterScreenUpdates(true)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)

            let tabBarController = storyboard.instantiateInitialViewController() as! UITabBarController
            tabBarController.tabBar.tintColor = UIColor.globalTintColor()
            self.window?.rootViewController = tabBarController
            self.updateNavigationBarAppeareance(UINavigationBarAppeareanceActive)
            
            tabBarController.view.addSubview(screenshotView)
            
            UIView.animateWithDuration(0.5, animations: {
                screenshotView.alpha = 0
            }, completion: { _ in
                screenshotView.removeFromSuperview()
            })
        }
    }
    
    func userDidSignOutNotification(notification: NSNotification) {
        let user = notification.object as! User
        user.delete()
        
        let screenshotView = self.window!.snapshotViewAfterScreenUpdates(false)
        let storyboard = UIStoryboard(name: "Auth", bundle: nil)

        let initialViewController = storyboard.instantiateInitialViewController()!
        self.window?.rootViewController = initialViewController
        initialViewController.view.addSubview(screenshotView)
        self.updateNavigationBarAppeareance(UINavigationBarAppeareanceActive)
        
        UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .TransitionCrossDissolve, animations: {
            screenshotView.alpha = 0
        } , completion: { _ in
            screenshotView.removeFromSuperview()
        })
        
    }
    
    class func sharedInstance() -> AppDelegate {
        return UIApplication.sharedApplication().delegate as! AppDelegate
    }

}


