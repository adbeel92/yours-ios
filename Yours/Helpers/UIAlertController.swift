//
//  UIAlertController.swift
//  Yours
//
//  Created by Eduardo Arenas Albarracin on 26/05/16.
//  Copyright © 2016 Eduardo Arenas. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showAlertWithTitle(controller: AnyObject?, title: String?, message: String?, handler: (Void -> Void)?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        let closeAction = UIAlertAction(title: "OK", style: .Cancel) { _ in
            alert.dismissViewControllerAnimated(true, completion: nil)
            handler?()
        }
        alert.addAction(closeAction)
        controller?.presentViewController(alert, animated: true, completion: nil)
    }
    
    func showAlertWithTitle(controller: AnyObject?, title: String?, message: String?) {
        showAlertWithTitle(controller, title: title, message: message, handler: nil)
    }
    
    func showAlertWithError(controller: AnyObject?, error: NSError, handler: (Void -> Void)?) {
        let title = error.localizedDescription
        let message = error.localizedFailureReason
        showAlertWithTitle(controller, title: title, message: message, handler: handler)
    }
    
    func showAlertWithError(controller: AnyObject?, error: NSError) {
        showAlertWithError(controller, error: error, handler: nil)
    }
    
}

class AlertHelper {
    
    static func showAlertWithTitle(controller: AnyObject?, title: String?, message: String? = "", handler: (Void -> Void)?) {
        let alert: UIAlertController = UIAlertController()
        alert.showAlertWithTitle(controller, title: title, message: message)
    }
    
    static func showAlertWithTitle(controller: AnyObject?, title: String?, message: String?) {
        showAlertWithTitle(controller, title: title, message: message, handler: nil)
    }
    
    static func showAlertWithError(controller: AnyObject?, error: NSError, handler: (Void -> Void)?) {
        let title = error.localizedDescription
        let message = error.localizedFailureReason
        showAlertWithTitle(controller, title: title, message: message, handler: handler)
    }
    
    static func showAlertWithError(controller: AnyObject?, error: NSError) {
        showAlertWithError(controller, error: error, handler: nil)
    }
    
}