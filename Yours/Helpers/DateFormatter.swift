//
//  DateFormatter.swift
//  Yours
//
//  Created by Eduardo Arenas Albarracin on 10/07/16.
//  Copyright © 2016 Eduardo Arenas. All rights reserved.
//

import Foundation

class DateFormatter {
    static func dateToString(date: NSDate) -> String {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSSZZZZ"
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        return formatter.stringFromDate(date)
    }
    static func stringToDate(string: String) -> NSDate {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSSZZZZ"
        formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        return formatter.dateFromString(string)!
    }
}