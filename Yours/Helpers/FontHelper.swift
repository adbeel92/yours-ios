//
//  FontHelper.swift
//  Spoonea
//
//  Created by Eduardo Arenas Albarracin on 6/02/16.
//  Copyright © 2016 Eduardo Arenas. All rights reserved.
//

import UIKit

class FontHelper {
    
//    SF UI Text
//        == SFUIText-Bold
//        == SFUIText-Regular
//        == SFUIText-Medium
//    SF UI Display
//        == SFUIDisplay-Light
//        == SFUIDisplay-Regular
//        == SFUIDisplay-Medium
    
    // MARK: SFUIText
    
    static func SFUITextRegularFontWithSize(size: CGFloat) -> UIFont {
        return UIFont(name: "SFUIText-Regular", size: size)!
    }
    
    static func SFUITextLightFontWithSize(size: CGFloat) -> UIFont {
        return UIFont(name: "SFUIText-Light", size: size)!
    }
    
    static func SFUITextMediumFontWithSize(size: CGFloat) -> UIFont {
        return UIFont(name: "SFUIText-Medium", size: size)!
    }
    
    // MARK: SFUIDisplay
    
    static func SFUIDisplayRegularFontWithSize(size: CGFloat) -> UIFont {
        return UIFont(name: "SFUIDisplay-Regular", size: size)!
    }
    
    static func SFUIDisplayMediumFontWithSize(size: CGFloat) -> UIFont {
        return UIFont(name: "SFUIDisplay-Medium", size: size)!
    }
    
    static func SFUIDisplayBoldFontWithSize(size: CGFloat) -> UIFont {
        return UIFont(name: "SFUIDisplay-Bold", size: size)!
    }
}
