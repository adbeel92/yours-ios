//
//  Array.swift
//  Yours
//
//  Created by Eduardo Arenas Albarracin on 2/07/16.
//  Copyright © 2016 Eduardo Arenas. All rights reserved.
//

import Foundation

extension Array {
    
    func groupBy<G: Hashable>(groupClosure: (Element) -> G) -> [G: [Element]] {
        var dictionary = [G: [Element]]()
        
        for element in self {
            let key = groupClosure(element)
            var array: [Element]? = dictionary[key]
            
            if (array == nil) {
                array = [Element]()
            }
            
            array!.append(element)
            dictionary[key] = array!
        }
        
        return dictionary
    }
}