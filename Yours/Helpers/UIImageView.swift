//
//  UIImageView.swift
//  Yours
//
//  Created by Eduardo Arenas Albarracin on 30/06/16.
//  Copyright © 2016 Eduardo Arenas. All rights reserved.
//

import UIKit

extension UIImageView {
    func setImageRounded(){
        self.layer.cornerRadius = self.frame.size.width / 2
        self.clipsToBounds = true
    }
}
