//
//  DefaultColor.swift
//  Spoonea
//
//  Created by Eduardo Arenas Albarracin on 7/02/16.
//  Copyright © 2016 Eduardo Arenas. All rights reserved.
//

import UIKit

extension UIColor {
    
    static func initWithRGB(red: CGFloat, green: CGFloat, blue: CGFloat) -> Self {
        return self.init(red: red/255, green: green/255, blue: blue/255, alpha: 1.0)
    }
    
    static func initWithRGB(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) -> Self {
        return self.init(red: red/255, green: green/255, blue: blue/255, alpha: alpha)
    }
    
    static func globalTintColor() -> Self {
        return self.init(red: 33/255, green: 184/255, blue: 213/255, alpha: 1)
    }
    
    static func defaultBlackColor() -> Self {
        return self.init(red: 74/255, green: 74/255, blue: 74/255, alpha: 1)
    }

    static func defaultGrayColor() -> Self
    {
        return self.init(red: 140/255, green: 140/255, blue: 140/255, alpha: 1)
    }
    
    static func anotherGrayColor() -> Self
    {
        return self.init(red: 152/255, green: 152/255, blue: 152/255, alpha: 1)
    }

    static func anotherBlueColor() -> Self
    {
        return self.init(red: 0/255, green: 122/255, blue: 255/255, alpha: 1)
    }
    
    static func anotherRedColor() -> Self
    {
        return self.init(red: 254/255, green: 60/255, blue: 47/255, alpha: 1)
    }
    
}