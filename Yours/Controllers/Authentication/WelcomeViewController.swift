//
//  WelcomeViewController.swift
//  Yours
//
//  Created by Eduardo Arenas Albarracin on 11/06/16.
//  Copyright © 2016 Eduardo Arenas. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {
    
    // MARK: Properties
    
    
    // MARK: Outlets
    
    @IBOutlet weak var centerSpaceConstraint: NSLayoutConstraint!
    
    // MARK: View Controller Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if DeviceType.IS_IPHONE_4_OR_LESS {
            self.centerSpaceConstraint.constant = 55
        } else if DeviceType.IS_IPHONE_5 {
            self.centerSpaceConstraint.constant = 45
        }
        if #available(iOS 9.0, *) {
            self.loadViewIfNeeded()
        } else {
            // Fallback on earlier versions
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK - Actions
    
    
}
