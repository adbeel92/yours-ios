//
//  ActivationViewContoller.swift
//  Yours
//
//  Created by Eduardo Arenas Albarracin on 11/06/16.
//  Copyright © 2016 Eduardo Arenas. All rights reserved.
//

import UIKit

class ActivationViewController: UIViewController {
    
    // MARK: Properties

    let userService: UserService = UserService()
    var user: User = User()
    
    // MARK: Outlets
    
    @IBOutlet weak var codeTextField: YoursTextField!
    @IBOutlet weak var resendCodeButton: UIButton!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var resendIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var sentToDescriptionLabel: UILabel!
    
    // MARK: View Controller Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.userService.delegate = self
        self.sentToDescriptionLabel.text = "We have sent you an SMS with a code to \(self.user.fullMobileNumber!). Please enter the activation code"
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        self.dismissKeyboard()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK - Actions
    
    @IBAction func viewTapped() {
        self.dismissKeyboard()
    }
    
    @IBAction func resendCodeButtonTapped() {
        self.resendCodeButton.highlighted = true
        self.resendCodeButton.enabled = false
        self.resendIndicatorView.startAnimating()
        let title = self.resendCodeButton.titleLabel?.text
        self.resendCodeButton.setTitle("", forState: .Normal)
        
        self.userService.resendActivationCode(self.user, success: { (user) -> Void in
            self.resendIndicatorView.stopAnimating()
            self.resendCodeButton.setTitle(title, forState: .Normal)
            AlertHelper.showAlertWithTitle(self, title: "Activation Code has been re-sent", handler: nil)
        }, failure: { (error, responseObject) -> Void in
            self.resendCodeButton.highlighted = false
            self.resendCodeButton.enabled = true
            self.resendIndicatorView.stopAnimating()
            self.resendCodeButton.setTitle(title, forState: .Normal)
        })
    }
    
    @IBAction func startButtonTapped() {
        self.startButton.highlighted = true
        self.startButton.enabled = false
        self.activityIndicatorView.startAnimating()
        let title = self.startButton.titleLabel?.text
        self.startButton.setTitle("", forState: .Normal)
        
        self.userService.validateActivationCode(self.user, activationCode: self.codeTextField.text!, success: { (user) -> Void in
            self.activityIndicatorView.stopAnimating()
            self.startButton.setTitle(title, forState: .Normal)
            NSNotificationCenter.defaultCenter().postNotificationName(User.UserDidSignInNotification, object: user)
        }, failure: { (error, responseObject) -> Void in
            self.startButton.highlighted = false
            self.startButton.enabled = true
            self.activityIndicatorView.stopAnimating()
            self.startButton.setTitle(title, forState: .Normal)
        })
    }
    
    func dismissKeyboard() {
        if self.codeTextField.isFirstResponder(){
            self.codeTextField.resignFirstResponder()
        }
    }
    
    // MARK: Navigation
    
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//    
//    }
}

extension ActivationViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if self.codeTextField.isFirstResponder() {
            self.codeTextField.resignFirstResponder()
        }
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        let currentCharacterCount = textField.text?.characters.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        let newLength = currentCharacterCount + string.characters.count - range.length
        if newLength <= 4 {
            self.startButton.enabled = (newLength == 4)
        }
        return newLength <= 4
    }
    
}
