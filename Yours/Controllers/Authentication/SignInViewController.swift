//
//  SignInViewController.swift
//  Yours
//
//  Created by Eduardo Arenas Albarracin on 11/06/16.
//  Copyright © 2016 Eduardo Arenas. All rights reserved.
//

import UIKit
import libPhoneNumber_iOS

// MARK: Form Model
struct LoginForm {
    var name: String?
    var phoneNumber: String?
    var countryCode: String?
}

class SignInViewController: UIViewController {
    
    // MARK: Properties
    
    var loginForm = LoginForm()
    var validator = Validator()
    let userService: UserService = UserService()
    var nbFormatter = NBAsYouTypeFormatter()
    var countryCodeSelected: String? {
        didSet {
            self.nbFormatter = NBAsYouTypeFormatter(regionCode: self.countryCodeSelected)
        }
    }
    
    // MARK: Outlets
    
    @IBOutlet weak var nameTextField: YoursTextField!
    @IBOutlet weak var phoneNumberTextField: YoursTextField!
    @IBOutlet weak var verifyButton: UIButton!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    // MARK: View Controller Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.userService.delegate = self
        self.countryCodeSelected = "PE"
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        self.dismissKeyboard()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK - Actions
    
    @IBAction func viewTapped() {
        self.dismissKeyboard()
    }
    
    @IBAction func verifyButtonTapped() {
        self.verifyButton.highlighted = true
        self.verifyButton.enabled = false
        self.activityIndicatorView.startAnimating()
        let title = self.verifyButton.titleLabel?.text
        self.verifyButton.setTitle("", forState: .Normal)

        loginForm.name = nameTextField.text
        loginForm.phoneNumber = phoneNumberTextField.text
        loginForm.countryCode = self.countryCodeSelected

        validator.loginForm = loginForm
        if validator.validateLoginFields() {
            let userTmp = User()
            userTmp.name = loginForm.name
            userTmp.mobileNumber = loginForm.phoneNumber
            self.userService.authenticate(userTmp, success: { (user, already_exists, verification_code) -> Void in
                self.verifyButton.highlighted = false
                self.verifyButton.enabled = true
                self.activityIndicatorView.stopAnimating()
                self.verifyButton.setTitle(title, forState: .Normal)
                self.performSegueWithIdentifier("VerifySegue", sender: user)
                if already_exists {
                    AlertHelper.showAlertWithTitle(self, title: "The number was registered (Temporal: verification code is \(verification_code))", handler: nil)
                } else {
                    AlertHelper.showAlertWithTitle(self, title: "(Temporal: verification code is \(verification_code))", handler: nil)
                }
            }, failure: { (error, responseObject) -> Void in
                self.verifyButton.highlighted = false
                self.verifyButton.enabled = true
                self.activityIndicatorView.stopAnimating()
                self.verifyButton.setTitle(title, forState: .Normal)
            })
        } else {
            self.verifyButton.highlighted = false
            self.verifyButton.enabled = true
            self.activityIndicatorView.stopAnimating()
            self.verifyButton.setTitle(title, forState: .Normal)
            showAlertWithError(self, error: validator.error!)
        }
    }
    
    func dismissKeyboard() {
        if self.nameTextField.isFirstResponder() {
            self.nameTextField.resignFirstResponder()
        } else if self.phoneNumberTextField.isFirstResponder() {
            self.phoneNumberTextField.resignFirstResponder()
        }
    }
    
    // MARK: Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "VerifySegue" {
            let activationCodeViewController: ActivationViewController = segue.destinationViewController as! ActivationViewController
            activationCodeViewController.user = sender as! User
        }
    }
}

extension SignInViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        if self.nameTextField.isFirstResponder() {
            self.phoneNumberTextField.becomeFirstResponder()
        }
        else {
            self.phoneNumberTextField.resignFirstResponder()
            self.verifyButton.sendActionsForControlEvents(.TouchUpInside)
        }
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneNumberTextField {
            if (range.length == 0) {
                textField.text = self.nbFormatter.inputDigit(string)
            }
            else if(range.length == 1) {
                textField.text = self.nbFormatter.removeLastDigit()
            }
            if (textField.text!.isEmpty) {
                self.verifyButton.enabled = false
            } else {
                self.verifyButton.enabled = self.nbFormatter.isSuccessfulFormatting
            }
            return false
        }
        return true
    }
    
}
