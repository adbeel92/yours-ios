//
//  ChatViewController.swift
//  Yours
//
//  Created by Eduardo Arenas Albarracin on 7/02/16.
//  Copyright © 2016 Eduardo Arenas. All rights reserved.
//

import UIKit

class ChatViewController: UIViewController {

    // MARK: Properties
    
    var chat: ChatEntity!
    var messages = [Message]()
    var currentUser: User = User.currentUser()!
    var bannerLabelTimer: NSTimer!
    var startedTyping: Bool = false
    var titleView: ChatTitleView = ChatTitleView()
    
    // MARK: Outlets
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tvMessageEditor: UITextView!
    @IBOutlet weak var conBottomEditor: NSLayoutConstraint!
    
    // MARK: - View Controller Cycle Life
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.titleView = NSBundle.mainBundle().loadNibNamed("ChatTitleView", owner: self, options: nil).first as! ChatTitleView
        self.titleView.userNameTitleLabel.text = chat.name
        self.titleView.conectionDetailLabel.text = self.chat.contacts.first!.lastConnectedDescription()
        self.navigationItem.titleView = titleView

        self.tableView.estimatedRowHeight = 50.0
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
        self.tvMessageEditor.delegate = self
        let swipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(ChatViewController.dismissKeyboard))
        swipeGestureRecognizer.direction = .Down
        swipeGestureRecognizer.delegate = self
        self.view.addGestureRecognizer(swipeGestureRecognizer)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController!.tabBar.hidden = true
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ChatViewController.handleKeyboardWillShowNotification(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ChatViewController.handleKeyboardWillHideNotification(_:)), name: UIKeyboardWillHideNotification, object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ChatViewController.handleConnectedUserNotification(_:)), name: UserWasConnectedNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ChatViewController.handleDisconnectedUserNotification(_:)), name: UserWasDisconnectedNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ChatViewController.handleUserMessageReceivedNotification(_:)), name: UserMessageReceivedNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ChatViewController.handleUserStartedTypingNotification(_:)), name: UserStartedTypingReceivedNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ChatViewController.handleUserStoppedTypingNotification(_:)), name: UserStoppedTypingReceivedNotification, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewDidAppear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIKeyboardWillHideNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UserWasConnectedNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UserWasDisconnectedNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UserMessageReceivedNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UserStartedTypingReceivedNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UserStoppedTypingReceivedNotification, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: IBAction Methods
    
    @IBAction func sendMessage(sender: AnyObject) {
        if tvMessageEditor.text.characters.count > 0 {
            SocketIOManager.sharedInstance.sendMessage(tvMessageEditor.text!, chatRoomKey: self.chat.room!)
            self.chat.saveIfNotExists()
            let _contact = ContactEntity()
            _contact.id = currentUser.id
            _contact.first_name = currentUser.name
            let message = Message(_body: tvMessageEditor.text, _sender: _contact, _socketRoom: self.chat.room!)
            self.reloadScrollWithNewMessage(message)
            tvMessageEditor.text = ""
            startedTyping = false
        }
    }
    
    // MARK: Methods
    
    func reloadScrollWithNewMessage(message: Message) {
        self.titleView.conectionDetailLabel.text = ""
        self.messages.append(message)
        self.tableView.reloadData()
        
        let delay = 0.1 * Double(NSEC_PER_SEC)
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(delay)), dispatch_get_main_queue()) { () -> Void in
            if self.messages.count > 0 {
                let lastRowIndexPath = NSIndexPath(forRow: self.messages.count - 1, inSection: 0)
                self.tableView.scrollToRowAtIndexPath(lastRowIndexPath, atScrollPosition: UITableViewScrollPosition.Bottom, animated: true)
            }
        }
    }
    
    func dismissKeyboard() {
        if tvMessageEditor.isFirstResponder() {
            tvMessageEditor.resignFirstResponder()
        }
    }
    
    // MARK: Keyboard Notification Methods
    
    func handleKeyboardWillShowNotification(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if let keyboardFrame = (userInfo[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
                conBottomEditor.constant = keyboardFrame.size.height
                view.layoutIfNeeded()
            }
        }
    }
    
    func handleKeyboardWillHideNotification(notification: NSNotification) {
        conBottomEditor.constant = 0
        view.layoutIfNeeded()
    }
    
    // MARK: Socket events Notification Methods
    
    func handleConnectedUserNotification(notification: NSNotification) {
        print("Connected UserNotification (chat controller)")
        print(notification.object)
        self.titleView.conectionDetailLabel.text = "Online now"
    }
    
    func handleDisconnectedUserNotification(notification: NSNotification) {
        print("Disconnected UserNotification (chat controller)")
        print(notification.object)
        if let dictionary = notification.object as? [String: AnyObject] {
            self.chat.contacts.first!.assignAttributes(dictionary, isFriend: true)
            self.chat.save()
            self.titleView.conectionDetailLabel.text = self.chat.contacts.first!.lastConnectedDescription()
        }
    }
    
    func handleUserMessageReceivedNotification(notification: NSNotification) {
        print("New message received (chat controller)")
        print(notification.object)
        if let dictionary = notification.object as? [String: AnyObject] {
            let message = Message(dictionary: dictionary)
            self.reloadScrollWithNewMessage(message)
        }
    }
    
    func handleUserStartedTypingNotification(notification: NSNotification) {
        print("Started Typing (chat controller)")
        print(notification.object)
        if let dictionary = notification.object as? [String: AnyObject] {
            if let _ = dictionary["from"]!["first_name"] as? String {
                self.titleView.conectionDetailLabel.text = "Typing..."
            }
        }
    }
    
    func handleUserStoppedTypingNotification(notification: NSNotification) {
        print("Stopped Typing (chat controller)")
        print(notification.object)
        if let _ = notification.object as? [String: AnyObject] {
            self.titleView.conectionDetailLabel.text = "Online now"
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}

// MARK: - UITextView Delegate

extension ChatViewController : UITextViewDelegate {
    func textViewShouldBeginEditing(textView: UITextView) -> Bool {
        return true
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if (range.location == 0 && text.isEmpty) {
            startedTyping = false
            SocketIOManager.sharedInstance.sendStopTypingMessage(self.chat.room!)
        } else if !startedTyping {
            startedTyping = true
            print("send STARTED typing")
            SocketIOManager.sharedInstance.sendStartTypingMessage(self.chat.room!)
        }
        return true
    }
}

// MARK: - UIGestureRecognizer Delegate

extension ChatViewController : UIGestureRecognizerDelegate {
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

// MARK: - UITableView DataSource

extension ChatViewController : UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let message = messages[indexPath.row]
        if message.sender.id == currentUser.id {
            let cell = tableView.dequeueReusableCellWithIdentifier("UserMessageCell", forIndexPath: indexPath) as! ChatMessageCell
            cell.loadUserMessage(message)
            return cell
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("OtherUserMessageCell", forIndexPath: indexPath) as! ChatMessageCell
            cell.loadOtherUserMessage(message)
            return cell
        }
    }
}

// MARK: - UITableView Delegate

extension ChatViewController : UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
}
