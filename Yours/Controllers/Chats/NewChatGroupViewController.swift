//
//  NewChatGroupViewController.swift
//  Yours
//
//  Created by Eduardo Arenas Albarracin on 2/07/16.
//  Copyright © 2016 Eduardo Arenas. All rights reserved.
//

import UIKit

class NewChatGroupViewController: UIViewController {
    
    // MARK: Properties
    
    
    // MARK: Outlets
    
    @IBOutlet weak var nextBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var groupNameTextField: YoursTextField!
    
    // MARK: View Controller Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.groupNameTextField.becomeFirstResponder()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        self.groupNameTextField.resignFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK - Actions
    
    @IBAction func cancelButtonTapped() {
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func nextButtonTapped() {
        
    }

}

extension NewChatGroupViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.nextButtonTapped()
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if textField == groupNameTextField {
            if (textField.text?.characters.count > 1 || (string.characters.count > 0 && string != "")) {
                self.nextBarButtonItem.enabled = true
            } else {
                self.nextBarButtonItem.enabled = false
            }
        }
        return true
    }
    
}
