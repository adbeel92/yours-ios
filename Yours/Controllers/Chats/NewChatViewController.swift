//
//  NewChatViewController.swift
//  Yours
//
//  Created by Eduardo Arenas Albarracin on 2/07/16.
//  Copyright © 2016 Eduardo Arenas. All rights reserved.
//

import UIKit
import MBProgressHUD

protocol NewChatViewControllerDelegate: class {
    func didOpenNewChat(chat: ChatEntity, animated: Bool)
}

class NewChatViewController: UIViewController {
    
    // MARK: Properties
    
    weak var delegate: NewChatViewControllerDelegate?
    let userService = UserService()
    let chatService = ChatService()
    var hud = MBProgressHUD()
    var contactSectionTitles = []
    var contactsDictionary: NSDictionary = [:] {
        didSet {
            self.contactSectionTitles = self.contactsDictionary.allKeys.sort({ (title, titleCompared) -> Bool in
                title.localizedCaseInsensitiveCompare(titleCompared as! String) == .OrderedAscending
            })
        }
    }
    let indexTitles = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]
    
    // MARK: Outlets
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: View Controller Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barStyle = .BlackTranslucent
        self.userService.delegate = self
        self.chatService.delegate = self
        self.contactsDictionary = ContactEntity.fetchDictionaryContacts()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK - Actions
    
    @IBAction func cancelButtonTapped() {
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - Navigation
    
    func openChat(chat: ChatEntity) {
        self.delegate?.didOpenNewChat(chat, animated: false)
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
}

extension NewChatViewController: UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1 + contactSectionTitles.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            let sectionTitle = contactSectionTitles[section - 1] as! String
            let sectionContacts = contactsDictionary[sectionTitle]
            return sectionContacts!.count
        }
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return ((section == 0) ? nil : (contactSectionTitles[section - 1] as! String))
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("DetailLinkCell", forIndexPath: indexPath)
            cell.textLabel!.text = "Create group"
            return cell
        } else {
            let sectionTitle = contactSectionTitles[indexPath.section - 1] as! String
            let sectionContacts = contactsDictionary[sectionTitle] as! [ContactEntity]
            let phoneContact = sectionContacts[indexPath.row]
            
            let cell = tableView.dequeueReusableCellWithIdentifier("ContactCell", forIndexPath: indexPath)
            let fullName: NSMutableAttributedString = NSMutableAttributedString()
            fullName.appendAttributedString(NSAttributedString(string: phoneContact.first_name!, attributes: [NSFontAttributeName: FontHelper.SFUITextRegularFontWithSize(14)]))
            fullName.appendAttributedString(NSAttributedString(string: " \(phoneContact.lastName!)", attributes: [NSFontAttributeName: FontHelper.SFUITextMediumFontWithSize(14)]))
            cell.textLabel!.attributedText = fullName
            return cell
        }
    }
    
    func sectionIndexTitlesForTableView(tableView: UITableView) -> [String]? {
        return indexTitles
    }
    
    func tableView(tableView: UITableView, sectionForSectionIndexTitle title: String, atIndex index: Int) -> Int {
        if title.isEmpty {
            return -1
        }
        return contactSectionTitles.indexOfObject(title)
    }
}

extension NewChatViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        if indexPath.section == 0 {
            self.performSegueWithIdentifier("NewChatGroupSegue", sender: nil)
        } else {
            let sectionTitle = contactSectionTitles[indexPath.section - 1] as! String
            let sectionContacts = contactsDictionary[sectionTitle] as! [ContactEntity]
            let phoneContact = sectionContacts[indexPath.row]
            
            hud = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            hud.labelText = "Loading..."
            hud.labelFont = FontHelper.SFUITextMediumFontWithSize(14)
            chatService.findOrCreateChatByPhoneContact(phoneContact, success: { (chat) in
                self.hud.hide(true)
                self.openChat(chat)
            }, failure: { (error, response) in
                self.hud.hide(true)
            })
        }
    }
}

