//
//  ChatsViewController.swift
//  Yours
//
//  Created by Eduardo Arenas Albarracin on 7/02/16.
//  Copyright © 2016 Eduardo Arenas. All rights reserved.
//

import UIKit

class ChatsViewController: UIViewController {
    
    // MARK: Properties
    
    var chats: [ChatEntity] = ChatEntity.getAll()
    
    // MARK: Outlets
    
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - View Controller Cycle Life
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadChats()
        self.navigationController?.navigationBar.barStyle = .BlackTranslucent
        
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
        
        SocketIOManager.sharedInstance.establishConnection()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ChatsViewController.handleUserMessageReceivedNotification(_:)), name: UserMessageReceivedNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ChatsViewController.handleUserStartedTypingNotification(_:)), name: UserStartedTypingReceivedNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ChatsViewController.handleUserStoppedTypingNotification(_:)), name: UserStoppedTypingReceivedNotification, object: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewDidAppear(animated)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UserMessageReceivedNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UserStartedTypingReceivedNotification, object: nil)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UserStoppedTypingReceivedNotification, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


    // MARK: - Navigation
    
    func openChat(chat: ChatEntity, animated: Bool) {
        if animated{
            self.performSegueWithIdentifier("ChatViewSegue", sender: chat)
        } else {
            self.performSegueWithIdentifier("ChatViewWithoutAnimationSegue", sender: chat)
        }
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let identifier = segue.identifier {
            if identifier == "ChatViewSegue" || identifier == "ChatViewWithoutAnimationSegue" {
                let chatViewController = segue.destinationViewController as! ChatViewController
                chatViewController.chat = sender as! ChatEntity
            } else if identifier == "NewChatViewSegue" {
                let navigationController = segue.destinationViewController as! UINavigationController
                let newChatViewController = navigationController.viewControllers.first as! NewChatViewController
                newChatViewController.delegate = self
            }
        }
    }
    
    // MARK: Methods
    
    func loadChats() {
//        tableView.reloadData()
        tableView.scrollEnabled = (self.chats.count != 0)
    }
    
    // MARK: Notification Methods
    
    func handleUserMessageReceivedNotification(notification: NSNotification) {
        print("New message received (users controller)")
        print(notification.object)
    }
    
    func handleUserStartedTypingNotification(notification: NSNotification) {
        print("Started Typing (users controller)")
        print(notification.object)
    }
    
    func handleUserStoppedTypingNotification(notification: NSNotification) {
        print("Stopped Typing (users controller)")
        print(notification.object)
    }
    
}

// MARK: - UITableView DataSource

extension ChatsViewController : UITableViewDataSource {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chats.count == 0 ? 1 : chats.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if chats.count == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier("EmptyCell", forIndexPath: indexPath)
            cell.selectionStyle = .None
            return cell
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier("ChatItemCell", forIndexPath: indexPath) as! ChatItemCell
            let chat = chats[indexPath.row] as ChatEntity
            cell.textLabel?.text = chat.title()
            cell.detailTextLabel?.text = "last message is here"
            
            return cell
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return chats.count == 0 ? (self.tableView.frame.size.height - 64 - 49) : 44
    }
}

// MARK: - UITableView Delegate

extension ChatsViewController : UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        if chats.count != 0 {
            let chat = chats[indexPath.row] as ChatEntity
            self.openChat(chat, animated: true)
        }
    }
}

// MARK: - NewChatView Delegate

extension ChatsViewController : NewChatViewControllerDelegate {
    func didOpenNewChat(chat: ChatEntity, animated: Bool) {
        openChat(chat, animated: animated)
    }
}
