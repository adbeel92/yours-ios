//
//  SettingsViewController.swift
//  Yours
//
//  Created by Eduardo Arenas Albarracin on 21/06/16.
//  Copyright © 2016 Eduardo Arenas. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    
    // MARK: Properties
    var currentUser = User.currentUser()
    var options = ["CurrentUserHeaderCell", "CurrentUserNameCell", "OnOfflineOptionCell", "DetailStyleCell", "HeaderStyleCell", "DetailStyleCell", "DetailStyleCell", "DetailStyleCell", "DetailStyleCell", "DetailedCell", "DetailRemovalLinkCell", "DetailRemovalLinkCell"]
    
    // MARK: Outlets
    
    
    
    // MARK: View Controller Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.barStyle = .BlackTranslucent
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.currentUser = User.currentUser()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK - Actions
    
    
}

extension SettingsViewController: UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 120
        case 2:
            return 117
        case 4:
            return 70
        case 9:
            return 166
        default:
            return 50
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let optionIdentifier = options[indexPath.row] as String
        
        let cell = tableView.dequeueReusableCellWithIdentifier(optionIdentifier, forIndexPath: indexPath)
        switch indexPath.row {
        case 1:
            cell.textLabel!.text = "Name:"
            cell.detailTextLabel!.text = currentUser?.name
        case 3:
            cell.textLabel!.text = "Themes"
        case 4:
            cell.textLabel!.text = "ACCOUNT"
        case 5:
            cell.textLabel!.text = "Invitations Accepted"
        case 6:
            cell.textLabel!.text = "Contact Us"
        case 7:
            cell.textLabel!.text = "Privacy Policy"
        case 8:
            cell.textLabel!.text = "Terms of Services"
        case 9:
            cell.textLabel!.text = "App Version"
            cell.detailTextLabel!.text = "1.1.9"
        case 10:
            cell.textLabel!.text = "Delete Account"
        case 11:
            cell.textLabel!.text = "Close session"
        default:
            cell.textLabel!.text = ""
        }
        cell.customize()
        return cell
    }
    
}

extension SettingsViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
}
