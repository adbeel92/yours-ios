//
//  SocketIOManager.swift
//  SocketChat
//
//  Created by Gabriel Theodoropoulos on 1/31/16.
//  Copyright © 2016 AppCoda. All rights reserved.
//

import UIKit
import SocketIOClientSwift

let UserWasConnectedNotification = "UserWasConnectedNotification"
let UserWasDisconnectedNotification = "UserWasDisconnectedNotification"
let UserMessageReceivedNotification = "UserMessageReceivedNotification"
let UserStartedTypingReceivedNotification = "UserStartedTypingReceivedNotification"
let UserStoppedTypingReceivedNotification = "UserStoppedTypingReceivedNotification"

class SocketIOManager: NSObject {
    static let sharedInstance = SocketIOManager()
    
    var socket: SocketIOClient = SocketIOClient(socketURL: NSURL(string: kChatHostURL)!, options: [SocketIOClientOption.ConnectParams(["access_token" : (User.currentUser()?.accessToken)!])])
    
    override init() {
        super.init()
        
    }
    
    func establishConnection() {
        if User.isUserSignedIn() {
            socket.on("connected") { (data, socketAck) -> Void in
                self.listenForEvents()
            }
            socket.on("notFound") { data, ack in
                NSNotificationCenter.defaultCenter().postNotificationName(User.UserDidSignOutNotification, object: User.currentUser())
                self.closeConnection()
            }
            socket.connect()
        }
    }
    
    func establishLogicalConnection() {
        socket.emit("logicalConnect")
    }
    
    func closeConnection() {
        socket.disconnect()
    }
    
    func closeLogicalConnection() {
        socket.emit("logicalDisconnect")
    }
    
    private func listenForEvents() {
        socket.on("userConnected") { (dataArray, socketAck) -> Void in
            NSNotificationCenter.defaultCenter().postNotificationName(UserWasConnectedNotification, object: dataArray[0] as! [String: AnyObject])
        }
        socket.on("userDisconnected") { (dataArray, socketAck) -> Void in
            NSNotificationCenter.defaultCenter().postNotificationName(UserWasDisconnectedNotification, object: dataArray[0] as! [String: AnyObject])
        }
        socket.on("message sent") { (dataArray, socketAck) -> Void in
            NSNotificationCenter.defaultCenter().postNotificationName(UserMessageReceivedNotification, object: dataArray[0] as? [String: AnyObject])
        }
        socket.on("started typing") { (dataArray, socketAck) -> Void in
            NSNotificationCenter.defaultCenter().postNotificationName(UserStartedTypingReceivedNotification, object: dataArray[0] as? [String: AnyObject])
        }
        socket.on("stopped typing") { (dataArray, socketAck) -> Void in
            NSNotificationCenter.defaultCenter().postNotificationName(UserStoppedTypingReceivedNotification, object: dataArray[0] as? [String: AnyObject])
        }
    }
    
    func sendMessage(message: String, chatRoomKey: String) {
        socket.emit("send message", message, chatRoomKey)
    }
    
    //    func getChatMessage(completionHandler: (messageInfo: [String: AnyObject]) -> Void) {
    //        socket.on("newChatMessage") { (dataArray, socketAck) -> Void in
    //            var messageDictionary = [String: AnyObject]()
    //            messageDictionary["nickname"] = dataArray[0] as! String
    //            messageDictionary["message"] = dataArray[1] as! String
    //            messageDictionary["date"] = dataArray[2] as! String
    //
    //            completionHandler(messageInfo: messageDictionary)
    //        }
    //    }
    
    func sendStartTypingMessage(chatRoomKey: String) {
        socket.emit("start typing", chatRoomKey)
    }
    
    
    func sendStopTypingMessage(chatRoomKey: String) {
        socket.emit("stop typing", chatRoomKey)
    }
}
