//
//  UserService.swift
//  Spoonea
//
//  Created by Eduardo Arenas Albarracin on 10/02/16.
//  Copyright © 2016 Eduardo Arenas. All rights reserved.
//

import Foundation

class UserService {
    
    let servicesManager: ServicesManager = ServicesManager()
    var delegate: AnyObject? = nil {
        didSet {
            self.servicesManager.delegate = delegate
        }
    }
    
    func authenticate(user: User, success: ((User!, Bool, String?) -> Void)!, failure: ((NSError!, AnyObject!) -> Void)? = nil) {
        let parameters : [String: Dictionary<String,String>] = ["user" : ["first_name" : user.name!, "country_code" : "pe", "mobile_number" : user.mobileNumber!]]
        self.servicesManager.post("/users", parameters: parameters, success: { (response, responseObject) -> Void in
            if let _ = responseObject["error"] as? [String: AnyObject] {
                self.servicesManager.handleResponseError(responseObject)
            } else {
                let _user : User = User(dictionary: responseObject["user"] as? Dictionary)
                let already_exists = responseObject["already_exists"] as? Bool
                success(_user, already_exists!, responseObject["verification_code"] as? String)
            }
        }, failure: { (error, responseObject, statusCode) -> Void in
            self.servicesManager.handleResponseError(responseObject)
            failure?(error, responseObject)
        })
    }
    
    func validateActivationCode(currentUser: User, activationCode: String, success: ((User!) -> Void)!, failure: ((NSError!, AnyObject!) -> Void)? = nil) {
        self.servicesManager.currentUser = currentUser
        self.servicesManager.post("/users/verify_code", parameters: ["code" : activationCode], success: { (response, responseObject) -> Void in
            if let _ = responseObject["error"] as? [String : AnyObject] {
                self.servicesManager.handleResponseError(responseObject)
            } else {
                let _user : User = User(dictionary: responseObject["user"] as? Dictionary)
                _user.save()
                success(_user)
            }
        }, failure: { (error, responseObject, statusCode) -> Void in
            self.servicesManager.handleResponseError(responseObject)
            failure?(error, responseObject)
        })
    }
    
    func resendActivationCode(currentUser: User, success: ((User!) -> Void)!, failure: ((NSError!, AnyObject!) -> Void)? = nil) {
        self.servicesManager.currentUser = currentUser
        self.servicesManager.post("/users/resend_code", parameters: nil, success: { (response, responseObject) -> Void in
            if let _ = responseObject["error"] as? [String : AnyObject] {
                self.servicesManager.handleResponseError(responseObject)
            } else {
                let _user : User = User(dictionary: responseObject["user"] as? Dictionary)
                success(_user)
            }
        }, failure: { (error, responseObject, statusCode) -> Void in
            self.servicesManager.handleResponseError(responseObject)
            failure?(error, responseObject)
        })
    }
    
}
