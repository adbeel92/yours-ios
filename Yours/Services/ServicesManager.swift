//
//  ServicesManager.swift
//  Yours
//
//  Created by Eduardo Arenas Albarracin on 26/05/16.
//  Copyright © 2016 Eduardo Arenas. All rights reserved.
//

import Foundation
import AFNetworking

let kChatHostURL: String = "https://yours-chat.herokuapp.com"
//let kChatHostURL: String = "http://localhost:5000"

let kHostURL: String = "https://yours-backend.herokuapp.com"
//let kHostURL: String = "http://localhost:3000"

let kAPIURL: String = "/api/v1"

class ServicesManager {
    
    var manager: AFURLSessionManager
    var currentUser: User?
    var delegate: AnyObject? = nil
    
    init() {
        let configuration: NSURLSessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        self.manager = AFURLSessionManager(sessionConfiguration: configuration)
        self.currentUser = User.currentUser()
    }
    
    func APIURLStringforPath(path: String) -> String {
        return kHostURL + kAPIURL + path;
    }
    
    // MARK - REST Methods
    
    func get(path: String, parameters: AnyObject?, success: ((NSURLResponse!, AnyObject!) -> Void)!, failure: ((
        NSError!, AnyObject!, Int!) -> Void)!) {
        let request: NSMutableURLRequest = AFJSONRequestSerializer().requestWithMethod("GET", URLString: APIURLStringforPath(path), parameters: parameters, error: nil)
        self.dataTaskWithRequest(request, success: success, failure: failure)
    }
    
    func post(path: String, parameters: AnyObject?, success: ((NSURLResponse!, AnyObject!) -> Void)!, failure: ((
        NSError!, AnyObject!, Int!) -> Void)!) {
        let request: NSMutableURLRequest = AFJSONRequestSerializer().requestWithMethod("POST", URLString: APIURLStringforPath(path), parameters: parameters, error: nil)
        self.dataTaskWithRequest(request, success: success, failure: failure)
    }
    
    func put(path: String, parameters: AnyObject?, success: ((NSURLResponse!, AnyObject!) -> Void)!, failure: ((
        NSError!, AnyObject!, Int!) -> Void)!) {
        let request: NSMutableURLRequest = AFJSONRequestSerializer().requestWithMethod("PUT", URLString: APIURLStringforPath(path), parameters: parameters, error: nil)
        self.dataTaskWithRequest(request, success: success, failure: failure)
    }
    
    func delete(path: String, parameters: AnyObject?, success: ((NSURLResponse!, AnyObject!) -> Void)!, failure: ((
        NSError!, AnyObject!, Int!) -> Void)!) {
        let request: NSMutableURLRequest = AFJSONRequestSerializer().requestWithMethod("DELETE", URLString: APIURLStringforPath(path), parameters: parameters, error: nil)
        self.dataTaskWithRequest(request, success: success, failure: failure)
    }
    
    func dataTaskWithRequest(request: NSMutableURLRequest, success: ((NSURLResponse!, AnyObject!) -> Void)!, failure: ((
        NSError!, AnyObject!, Int!) -> Void)!) {
        print("request \(request)")
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        if (currentUser != nil) {
            request.setValue(currentUser!.accessToken, forHTTPHeaderField: "Authorization")
        }
        let dataTask: NSURLSessionDataTask = self.manager.dataTaskWithRequest(request) { (response, responseObject, error) -> Void in
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            if (error != nil){
                print("error -> \(error)")
                print("responseObject \(responseObject)")
                if let _response = response as? NSHTTPURLResponse {
                    print("status code: \(_response.statusCode)")
                    if (_response.statusCode == 401) {
                        NSNotificationCenter.defaultCenter().postNotificationName(User.UserDidSignOutNotification, object: self.currentUser)
                    } else {
                        if (failure != nil) {
                            failure?(error, responseObject, _response.statusCode)
                        } else {
                            self.handleResponseError(responseObject)
                        }
                    }
                }
            } else {
                print("response \(response)")
                print("responseObject \(responseObject)")
                success?(response, responseObject)
            }
        }
        dataTask.resume()
    }
    
    // MARK - Failure handler
    
    func handleResponseError(responseObject: AnyObject?) {
        if let resposeError = responseObject!["error"] as? [String : AnyObject] {
            let error: NSError = handleSuccessResponseWithStatusError(resposeError["title"] as! String, failureReason: resposeError["reason"] as? String, recoverySuggestion: resposeError["suggestion"] as? String)
            AlertHelper.showAlertWithError(self.delegate, error: error)
        }
    }
    
    func handleSuccessResponseWithStatusError(description: String, failureReason: String? = "", recoverySuggestion: String? = "") -> NSError {
        let userInfo: Dictionary = [
            NSLocalizedDescriptionKey: NSLocalizedString(description, comment: ""),
            NSLocalizedFailureReasonErrorKey: NSLocalizedString(failureReason!, comment: ""),
            NSLocalizedRecoverySuggestionErrorKey: NSLocalizedString(recoverySuggestion!, comment: "")]
        return NSError(domain: "com.spoonea.request_error", code: 1, userInfo: userInfo)
    }
    
}