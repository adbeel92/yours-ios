//
//  ChatService.swift
//  Yours
//
//  Created by Eduardo Arenas Albarracin on 2/07/16.
//  Copyright © 2016 Eduardo Arenas. All rights reserved.
//

import Foundation

class ChatService {
    
    let servicesManager: ServicesManager = ServicesManager()
    var delegate: AnyObject? = nil {
        didSet {
            self.servicesManager.delegate = delegate
        }
    }
    
    func findOrCreateChatByPhoneContact(phoneContact: ContactEntity, success: ((ChatEntity!) -> Void)!, failure: ((NSError!, AnyObject!) -> Void)? = nil) {
        let parameters = ["contact_phones" : phoneContact.phoneNumbers]
        print (parameters)
        self.servicesManager.post("/chats/exists", parameters: parameters, success: { (response, responseObject) -> Void in
            if let _ = responseObject["error"] as? [String : AnyObject] {
                self.servicesManager.handleResponseError(responseObject)
            } else {
                let _chat = ChatEntity(dictionary: responseObject["chat"] as? Dictionary)
                _chat.name = phoneContact.fullName()
                _chat.contacts = [ContactEntity(dictionary: responseObject["user"] as? Dictionary)]
                success(_chat)
            }
            }, failure: { (error, responseObject, statusCode) -> Void in
                failure?(error, responseObject)
        })
    }
    
}