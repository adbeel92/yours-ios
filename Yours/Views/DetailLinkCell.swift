//
//  DetailLinkCell.swift
//  Yours
//
//  Created by Eduardo Arenas on 6/27/16.
//  Copyright © 2016 Eduardo Arenas. All rights reserved.
//

import UIKit

class DetailLinkCell: DetailStyleCell {
    override func customize() {
        self.customTitleLabel.textColor = UIColor.anotherBlueColor()
        self.customTitleLabel.font = FontHelper.SFUITextRegularFontWithSize(17)
    }
}