//
//  DetailStyleCell.swift
//  Yours
//
//  Created by Eduardo Arenas Albarracin on 29/06/16.
//  Copyright © 2016 Eduardo Arenas. All rights reserved.
//

import UIKit

class DetailStyleCell: UITableViewCell {
    
    @IBOutlet weak var customTitleLabel: UILabel!
    
//    override init(style: UITableViewCellStyle, reuseIdentifier: String!) {
//        super.init(style: ., reuseIdentifier: reuseIdentifier)
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
    
    override func customize() {
        self.customTitleLabel.textColor = UIColor.defaultBlackColor()
        self.customTitleLabel.font = FontHelper.SFUITextRegularFontWithSize(17)
    }
}
