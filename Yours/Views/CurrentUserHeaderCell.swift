//
//  CurrentUserHeaderCell.swift
//  Yours
//
//  Created by Eduardo Arenas Albarracin on 29/06/16.
//  Copyright © 2016 Eduardo Arenas. All rights reserved.
//

import UIKit

class CurrentUserHeaderCell: UITableViewCell {
    
    @IBOutlet weak var customImageView: UIImageView!
    @IBOutlet weak var editCustomImageViewButton: UIButton!
    @IBOutlet weak var promptTextView: UILabel!
    
    override func customize() {
        self.customImageView.setImageRounded()
        self.selectionStyle = .None
    }
}
