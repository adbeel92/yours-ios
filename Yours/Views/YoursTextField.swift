//
//  SpooneaTextField.swift
//  Spoonea
//
//  Created by Eduardo Arenas Albarracin on 7/02/16.
//  Copyright © 2016 Eduardo Arenas. All rights reserved.
//

import UIKit

class YoursTextField: UITextField {
    
    let padding = UIEdgeInsets(top: 7, left: 0, bottom: 7, right: 0)
    var optionsEnabled: Bool = true
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // Set border bottom
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.anotherGrayColor().CGColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        self.layer.addSublayer(border)
        
        // Set font
        self.font = FontHelper.SFUITextMediumFontWithSize(14)
        
        // Set text and placeholder color
        self.textColor = UIColor.defaultBlackColor()
        let placeholderColor = UIColor.defaultGrayColor()
        self.setValue(placeholderColor, forKeyPath: "_placeholderLabel.textColor")

    }
    
    override func textRectForBounds(bounds: CGRect) -> CGRect {
        return self.newBounds(bounds)
    }
    
    override func placeholderRectForBounds(bounds: CGRect) -> CGRect {
        return self.newBounds(bounds)
    }
    
    override func editingRectForBounds(bounds: CGRect) -> CGRect {
        return self.newBounds(bounds)
    }
    
    override func canPerformAction(action: Selector, withSender sender: AnyObject?) -> Bool {
        if !optionsEnabled {
            return false
        }
        return super.canPerformAction(action, withSender: sender)
    }
    
    private func newBounds(bounds: CGRect) -> CGRect {
        var newBounds = bounds
        newBounds.origin.x += padding.left
        newBounds.origin.y += padding.top
        newBounds.size.height -= padding.top + padding.bottom
        newBounds.size.width -= padding.left + padding.right
        return newBounds
    }
}
