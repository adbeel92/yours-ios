//
//  HeaderStyleCell.swift
//  Yours
//
//  Created by Eduardo Arenas Albarracin on 29/06/16.
//  Copyright © 2016 Eduardo Arenas. All rights reserved.
//

import UIKit

class HeaderStyleCell: UITableViewCell {
    @IBOutlet weak var customTitleLabel: UILabel!
    
    override func customize() {
        self.customTitleLabel.textColor = UIColor.defaultBlackColor()
        self.customTitleLabel.font = FontHelper.SFUITextMediumFontWithSize(17)
        self.selectionStyle = .None
    }
}
