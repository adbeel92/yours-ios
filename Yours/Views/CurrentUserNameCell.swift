//
//  CurrentUserNameCell.swift
//  Yours
//
//  Created by Eduardo Arenas Albarracin on 29/06/16.
//  Copyright © 2016 Eduardo Arenas. All rights reserved.
//

import UIKit

class CurrentUserNameCell: UITableViewCell {
    
    override func customize() {
        self.selectionStyle = .None
    }
    
}
