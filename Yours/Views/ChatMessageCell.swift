//
//  ChatMessageCell.swift
//  SocketChat
//
//  Created by Eduardo Arenas Albarracin on 7/02/16.
//  Copyright © 2016 Eduardo Arenas. All rights reserved.
//

import UIKit

class ChatMessageCell: BaseCell {

    @IBOutlet weak var chatMessageTextView: UITextView!
    @IBOutlet weak var messageDetailsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func loadUserMessage(message: Message) {
        self.chatMessageTextView.text = message.body
        self.messageDetailsLabel.text = "20:02"
    }
    
    func loadOtherUserMessage(message: Message) {
        self.chatMessageTextView.text = message.body
        self.messageDetailsLabel.text = "15:51"
    }
    
    func loadOtherUserGroupMessage(message: Message) {
        self.chatMessageTextView.text = message.body
        self.messageDetailsLabel.text = "15:51"
    }

}
