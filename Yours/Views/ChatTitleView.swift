//
//  ChatTitleView.swift
//  Yours
//
//  Created by Eduardo Arenas on 7/5/16.
//  Copyright © 2016 Eduardo Arenas. All rights reserved.
//

import UIKit

class ChatTitleView: UIView {
    
    @IBOutlet weak var userNameTitleLabel: UILabel!
    @IBOutlet weak var conectionDetailLabel: UILabel!
    
}
