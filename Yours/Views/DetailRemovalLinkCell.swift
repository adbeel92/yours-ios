//
//  DetailRemovalLinkCell.swift
//  Yours
//
//  Created by Eduardo Arenas Albarracin on 29/06/16.
//  Copyright © 2016 Eduardo Arenas. All rights reserved.
//

import UIKit

class DetailRemovalLinkCell: DetailStyleCell {
    override func customize() {
        self.customTitleLabel.textColor = UIColor.anotherRedColor()
        self.customTitleLabel.font = FontHelper.SFUITextRegularFontWithSize(17)
    }
}
