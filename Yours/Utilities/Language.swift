//
//  Language.swift
//  Spoonea
//
//  Created by Eduardo Arenas Albarracin on 9/05/16.
//  Copyright © 2016 Eduardo Arenas. All rights reserved.
//

import Foundation

class Language {
    static func current() -> String {
        if NSBundle.mainBundle().preferredLocalizations.first! as NSString == "es" {
            return "ES"
        } else {
            return "EN"
        }
    }
}