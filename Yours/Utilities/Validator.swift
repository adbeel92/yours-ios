//
//  Validator.swift
//  Spoonea
//
//  Created by Eduardo Arenas Albarracin on 13/02/16.
//  Copyright © 2016 Eduardo Arenas. All rights reserved.
//

import Foundation
import libPhoneNumber_iOS

let EMAIL_REGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"

class Validator {
    
    var error: NSError?
    
    var loginForm: LoginForm!
//    var signUpForm: SignUpForm!
//    var profileForm: ProfileForm!
    
    // Validate forms
    
    func validateLoginFields() -> Bool {
        let nameError = validateName(loginForm.name)
        let phoneNumberError = validatePhoneNumber(loginForm.phoneNumber, countryCode: loginForm.countryCode)

        if (nameError == nil) && (phoneNumberError == nil) {
            self.error = nil
            return true
        }
        else {
            var errorDescription = ""
            
            if let emailErrorDesc = nameError?.localizedDescription {
                errorDescription += emailErrorDesc + ". "
            }
            if let passwordErrorDesc = phoneNumberError?.localizedDescription {
                errorDescription += passwordErrorDesc + ". "
            }
            
            self.error = NSError(domain: "com.yours.validator", code: 10, userInfo: [NSLocalizedDescriptionKey: errorDescription])
            return false
        }
    }
    
//    func validateProfileFields() -> Bool {
//        let firstNameError = validateRequiredString(profileForm.firstName, fieldName: "First Name")
//        let lastNameError = validateRequiredString(profileForm.lastName, fieldName: "Last Name")
//        let emailError = validateEmail(profileForm.email)
//        
//        if (firstNameError == nil) && (lastNameError == nil) && (emailError == nil) {
//            self.error = nil
//            return true
//        } else {
//            var errorDescription = ""
//            
//            if let firstNameErrorDesc = firstNameError?.localizedDescription {
//                errorDescription += firstNameErrorDesc + ". "
//            }
//            if let lastNameErrorDesc = lastNameError?.localizedDescription {
//                errorDescription += lastNameErrorDesc + ". "
//            }
//            if let emailErrorDesc = emailError?.localizedDescription {
//                errorDescription += emailErrorDesc + ". "
//            }
//            
//            self.error = NSError(domain: "com.spoonea.validator", code: 20, userInfo: [NSLocalizedDescriptionKey: errorDescription])
//            return false
//        }
//    }
//
//    func validateSignUpFields() -> Bool {
//        let firstNameError = validateRequiredString(signUpForm.firstName, fieldName: "First Name")
//        let lastNameError = validateRequiredString(signUpForm.lastName, fieldName: "Last Name")
//        let emailError = validateEmail(signUpForm.email)
//        let rePasswordError = validatePasswordAndRePassword(signUpForm.password, rePassword: signUpForm.rePassword)
//        
//        if (firstNameError == nil) && (lastNameError == nil) && (emailError == nil) && (rePasswordError == nil) {
//            self.error = nil
//            return true
//        } else {
//            var errorDescription = ""
//            
//            if let firstNameErrorDesc = firstNameError?.localizedDescription {
//                errorDescription += firstNameErrorDesc + ". "
//            }
//            if let lastNameErrorDesc = lastNameError?.localizedDescription {
//                errorDescription += lastNameErrorDesc + ". "
//            }
//            if let emailErrorDesc = emailError?.localizedDescription {
//                errorDescription += emailErrorDesc + ". "
//            }
//            if let rePasswordErrorDesc = rePasswordError?.localizedDescription {
//                errorDescription += rePasswordErrorDesc + ". "
//            }
//            
//            self.error = NSError(domain: "com.spoonea.validator", code: 20, userInfo: [NSLocalizedDescriptionKey: errorDescription])
//            return false
//        }
//    }
    
    // Validators
    
    func validateName(name: String?) -> NSError? {
        guard let unwrappedName = name where unwrappedName.isEmpty == false else {
            return NSError(domain: "com.yours.validator", code: 1, userInfo: [NSLocalizedDescriptionKey: "Please enter your Name"])
        }
        return nil
    }
    
    func validatePhoneNumber(phoneNumber: String?, countryCode: String?) -> NSError? {
        let nbPhoneNumberUtil = NBPhoneNumberUtil()
        var nbPhoneNumber = NBPhoneNumber()
        do {
            nbPhoneNumber = try nbPhoneNumberUtil.parse(phoneNumber, defaultRegion: countryCode)
            let valid = nbPhoneNumberUtil.isValidNumberForRegion(nbPhoneNumber, regionCode: countryCode)
//            let formattedString: String = try nbPhoneNumberUtil.format(nbPhoneNumber, numberFormat: .INTERNATIONAL)
            return valid ? nil : NSError(domain: "com.yours.validator", code: 2, userInfo: [NSLocalizedDescriptionKey: "Phone number is invalid"])
        } catch let error as NSError {
            print(error.localizedDescription)
            return error
        }
    }
    
//    func validatePasswordAndRePassword(password: String?, rePassword: String?) -> NSError? {
//        let passwordError = validatePassword(password)
//        
//        if (passwordError == nil) {
//            if (password == rePassword) {
//                return nil
//            } else {
//                return NSError(domain: "com.spoonea.validator", code: 5, userInfo: [NSLocalizedDescriptionKey: "Password and Re-Password must be equal"])
//            }
//        } else {
//            return passwordError
//        }
//    }
//    
//    func validateRequiredString(inputString: String!, fieldName: String) -> NSError? {
//        switch inputString {
//        case .None:
//            return NSError(domain: "com.spoonea.validator", code: 6, userInfo: [NSLocalizedDescriptionKey: "\(fieldName) is required"])
//        case .Some:
//            if let stringValue = inputString {
//                if (stringValue.isEmpty) {
//                    return NSError(domain: "com.spoonea.validator", code: 6, userInfo: [NSLocalizedDescriptionKey: "\(fieldName) is required"])
//                }
//            }
//            return nil
//        }
//    }
    
    // Regex validations
    
    func hasCorrectPattern(inputText: String, pattern: String) -> Bool {
        return NSPredicate(format: "SELF MATCHES %@", pattern).evaluateWithObject(inputText)
    }
}